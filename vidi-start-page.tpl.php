<?php
// $Id:

drupal_add_css(drupal_get_path('module', 'vidi') . '/css/chose_type.css');
?>

<div class="start_page">
  <?php

// $Id:
  echo t('<p>For an overview of available visualization types click "Select Visualization Type". The wizard will guide you through the visualization tools, including interactive example displays and detailed information on required data formats for each visualization type.<br/>

If you already have prepared datasets, click the "Upload your data" button. After upload, you will be offered a selection of visualization types to choose from, leading to an interactive display where you can tweak the way your data is visually presented. <br/>

For registered and logged in users we offer the option to permanently store created displays and to export them in an embeddable format that can be used on other websites, in news articles and blog posts.</p>

');

  echo "<div class=\"start_page_images\">";
  echo l("<img alt=\"Select Visualization Type\" src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/select_visualization_type.jpg\" >", "vidi/chose_visualization_type", array('html'=>TRUE));
  echo l("<img alt=\"Upload Your Data\" src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/upload_your_data.jpg\" >", 'importer', array('html'=>TRUE, 'query'=>array('return_link'=>'vidi/chose_visualization_type')));
  echo "</div>";
  ?>
</div>