<?php

/**
 * @file
 * Form which controls the Google Visualizations
 */

function vidi_gvs_form($form_state) {
  if (!module_exists('gvs')) {
    drupal_set_message(t('To use Google Visualizations, please download the gvs module from <a href="@gvs-module-download">here</a>', array('@gvs-module-download' => 'http://drupal.org/project/gvs')));
    drupal_goto('vidi/chose_visualization_type');
  }
  drupal_add_css(drupal_get_path('module', 'vidi') . '/css/vidi-gvs.css');
  global $base_url;
  $gvs_gapi="http://www.google.com/jsapi";
  drupal_set_html_head('<script type="text/javascript" src="' . $gvs_gapi  . '"></script>');

  $api_key = variable_get('dv_api_key', False);
  if ($api_key) {
    $gvs_gmap_gapi="http://maps.google.com/maps?file=api&v=2&key=" . $api_key;
    drupal_set_html_head('<script type="text/javascript" src="' . $gvs_gmap_gapi  . '"></script>');
  }
  else {
    drupal_set_message(t('Enter Google maps key in Gmap settings.'), 'warning');
  }

  $nekiurl=url('vidi/cb');
  $tablename = db_escape_table(arg(2));

  $all_fields["NULL"]="----";
  $numeric_fields["NULL"]="----";
  $integer_fields["NULL"]="----";
  $date_fields["NULL"]="----";
  $other_fields["NULL"]="----";
  $label_fields["NULL"]="----";
  if ($tablename !='') {
    if (db_table_exists($tablename)) {
      $results = db_query("show columns from {$tablename}");
      while ($result = db_fetch_object($results)) {
        $all_fields[$result->Field] = $result->Field;
        if ($result->Type == "float" || substr($result->Type, 0, 3) == "int" || substr($result->Type, 0, 4) == "doub") {
          $numeric_fields[$result->Field] = $result->Field;
          if (substr($result->Type, 0, 3) == "int") {
            $label_fields[$result->Field] = $result->Field;
            $integer_fields[$result->Field] = $result->Field;
          }
        }
        elseif ($result->Type == "datetime" || $result->Type == "timestamp") {
          $date_fields[$result->Field] = $result->Field;
        }
        else {
          $other_fields[$result->Field] = $result->Field;
          $label_fields[$result->Field] = $result->Field;
        }
      }
    }
  }
  
  $update = 0;
  $filename = "NULL";
  if (isset($_GET['fid'])) {
    $embedingno = db_query_range("SELECT * FROM {gvs_embeds} WHERE fid = %d", $_GET['fid'], 0, 1);
    $row_obj = db_fetch_object($embedingno);
    $defaults = unserialize($row_obj->form_state_storage);
    $embedingno1 = db_query_range("SELECT * FROM {files} WHERE fid = %d", $_GET['fid'], 0, 1);
    $row_obj1 = db_fetch_object($embedingno1);
    $filename = $row_obj1->filename;
    $update = 1;
  }

  if (isset($_GET['datasource'])) {
   $requiredviz = $_GET['datasource'];
  }

  $external_js_file = file_get_contents(drupal_get_path('module', 'vidi') . '/js/vidi_gvs_form_display.js');
  $for_replacement = array("nekiurl", "tablename", "requiredviz", "update_for_replacment", "filename_for_replacment");
  $replace_with   = array('"' . $nekiurl . '"', $tablename, '"' . $requiredviz . '"', $update, $filename);
  $js_with_data = str_replace($for_replacement, $replace_with, $external_js_file);
  drupal_add_js($js_with_data, 'inline');

  $form['addscrip']=array(
    '#type' => 'item',
    '#value' => '',
    '#prefix' => '<div id="addscrip">',
    '#suffix' => '</div>',
  );

  $image_path = base_path() . path_to_theme() . '/images/try_another_type_s.jpg';
  if (file_exists($image_path)) {
    $link_argument = theme_image("$image_path");
  }
  else {
    $link_argument = t("Try another visualization type");
  }
  
  if ($_GET['new_data'] == 'TRUE') {
    $link = l($link_argument, 'vidi/chose_visualization_type', array('html' => TRUE, 'query' => 'tablename=' . $form_state['storage']['tablename']));
  }
  else {
    $link = l($link_argument, 'vidi/chose_visualization_type', array('html' => TRUE));
  }
  if (user_access('visualize data')) {
    $form['chart_div']=array(
      '#type' => 'item',
      '#value' => '',
      '#prefix' => '<div id="chart_div" style="height:400px;width:600px;">',
      '#suffix' => '</div><div id="embedinstructons"></div><div style="float:left;"><a id="embedlink">[embed]</a></div><div id = "back-button">' . $link . '</div><div class = "clear"></div>',
    );
  }
  else {
    $form['chart_div']=array(
      '#type' => 'item',
      '#value' => '',
      '#prefix' => '<div id="vidi-chart-div">',
      '#suffix' => '</div><div id = "back-button">' . $link . '</div><div class = "clear"></div>',
    );
  }
  $form['tablename'] = array(
    '#type' => 'hidden',
    '#value' => $tablename,
  );

  $form['width'] = array(
    '#type' => 'hidden',
    '#value' => '300',
  );

  $form['height'] = array(
    '#type' => 'hidden',
    '#value' => '200',
  );

  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['data']['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#weight' => 1,
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#default_value' => isset($defaults['title'])?$defaults['title']:'',
  );

  $form['data']['type'] = array(
    '#type' => 'hidden',
    '#value' => (string)($requiredviz),
  );

  $form['data']['geomaptype'] = array(
    '#title' => t('Map Type'),
    '#weight' => 6,
    '#type' => 'select',
    '#options' => array('markers' => 'Markers', 'regions' => 'Regions'),
    '#default_value' => isset($defaults['geomaptype'])?$defaults['geomaptype']:'regions',
    '#prefix' => '<div class="map1">',
    '#suffix' => '</div>',
  );

  $form['data']['geomaplat'] = array(
    '#title' => t('Latitude'),
    '#description' => t('Must be number specifying location latitude. Precision beyond the 6 decimal places is ignored.'),
    '#type' => 'select',
    '#weight' => 7,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['geomaplat'])?$defaults['geomaplat']:'NULL',
    '#prefix' => '<div class="map1">',
    '#suffix' => '</div>',
  );

  $form['data']['geomaplon'] = array(
    '#title' => t('Longitude'),
    '#description' => t('Must be number specifying location longitude. Precision beyond the 6 decimal places is ignored.'),
    '#type' => 'select',
    '#weight' => 8,
    '#default_value' => isset($defaults['geomaplon'])?$defaults['geomaplon']:'NULL',
    '#options' => $numeric_fields,
    '#prefix' => '<div class="map1">',
    '#suffix' => '</div>',
  );

  $form['data']['geomaplat1'] = array(
    '#title' => t('Latitude'),
    '#description' => t('Must be number specifying location latitude. Precision beyond the 6 decimal places is ignored.'),
    '#type' => 'select',
    '#weight' => 9,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['geomaplat1']) ? $defaults['geomaplat1']:'NULL',
    '#prefix' => '<div class="map2">',
    '#suffix' => '</div>',
  );

  $form['data']['geomaplon1'] = array(
    '#title' => t('Longitude'),
    '#description' => t('Must be number specifying location longitude. Precision beyond the 6 decimal places is ignored.'),
    '#type' => 'select',
    '#weight' => 10,
    '#default_value' => isset($defaults['geomaplon1'])?$defaults['geomaplon1']:'NULL',
    '#options' => $numeric_fields,
    '#prefix' => '<div class="map2">',
    '#suffix' => '</div>',
  );

  $form['data']['geomapstring'] = array(
    '#title' => t('Adress or region'),
    '#description' => t('Must be string(A specific address, uppercase ISO-3166 code, ISO-3166-2 region code, a metropolitan area code-US)'),
    '#type' => 'select',
    '#default_value' => isset($defaults['geomapstring'])?$defaults['geomapstring']:'NULL',
    '#weight' => 11,
    '#options' => $other_fields,
  );

  $form['data']['maptype'] = array(
    '#title' => t('Map type'),
    '#type' => 'select',
    '#weight' => 12,
    '#options' => array(
      'normal' => t('Normal'),
      'satellite' => t('Satellite'),
      'hybrid' => t('Hybrid')
    ),
    '#default_value' => 'normal',
  );

  $form['data']['xaxis'] = array(
    '#title' => t('Label'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 13,
    '#options' => $label_fields,
    '#required' => TRUE,
    '#default_value' => isset($defaults['xaxis'])?$defaults['xaxis']:'NULL',
  );

  $form['data']['xaxis1'] = array(
    '#title' => t('X-axis'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 14,
    '#options' => $numeric_fields,
    '#required' => TRUE,
    '#default_value' => isset($defaults['xaxis1'])?$defaults['xaxis1']:'NULL',
  );

  $form['data']['yaxis11'] = array(
    '#title' => t('Date'),
    '#description' => t('Must be DATE'),
    '#type' => 'select',
    '#weight' => 15,
    '#options' => $all_fields,
    '#required' => TRUE,
    '#default_value' => isset($defaults['yaxis11'])?$defaults['yaxis11']:'NULL',
  );

  $form['data']['mcdateformat'] = array(
    '#title' => t('Date format'),
    '#type' => 'select',
    '#options' => array(
      '1' => t('Date (m-d-Y)'),
      '2' => t('Year'),
      '3' => t('Week number'),
      '4' => t('Quarter'),
    ),
    '#default_value' => isset($defaults['mcdateformat'])?$defaults['mcdateformat']:'1',
    '#weight' => 15,
  );

  $form['data']['yaxis1'] = array(
    '#title' => t('Value-1'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 16,
    '#options' => $numeric_fields,
    '#required' => TRUE,
    '#default_value' => isset($defaults['yaxis1'])?$defaults['yaxis1']:'NULL',
  );

  $form['data']['zaxis1'] = array(
    '#title' => t('String Value-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 17,
    '#options' => $other_fields,
    '#default_value' => isset($defaults['zaxis1'])?$defaults['zaxis1']:'NULL',
  );

  $form['data']['zzaxis1'] = array(
    '#title' => t('String Value-1-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 18,
    '#options' => $other_fields,
    '#prefix' => '<div class="zzaxis">',
    '#suffix' => '</div>',
    '#default_value' => isset($defaults['zzaxis1'])?$defaults['zzaxis1']:'NULL',
  );

  $form['data']['yaxis2'] = array(
    '#title' => t('Value-2'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 19,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['yaxis2'])?$defaults['yaxis2']:'NULL',
  );

  $form['data']['zaxis2'] = array(
    '#title' => t('String Value-2'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 20,
    '#options' => $other_fields,
    '#default_value' => isset($defaults['zaxis2'])?$defaults['zaxis2']:'NULL',
  );

  $form['data']['zzaxis2'] = array(
    '#title' => t('String Value-2-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 21,
    '#options' => $other_fields,
    '#prefix' => '<div class="zzaxis">',
    '#suffix' => '</div>',
    '#default_value' => isset($defaults['zzaxis2'])?$defaults['zzaxis2']:'NULL',
  );

  $form['data']['yaxis3'] = array(
    '#title' => t('Value-3'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 22,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['yaxis3'])?$defaults['yaxis3']:'NULL',
  );

  $form['data']['zaxis3'] = array(
    '#title' => t('String Value-3'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 23,
    '#options' => $other_fields,
    '#default_value' => isset($defaults['zaxis3'])?$defaults['zaxis3']:'NULL',
  );

  $form['data']['zzaxis3'] = array(
    '#title' => t('String Value-3-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 24,
    '#options' => $other_fields,
    '#prefix' => '<div class="zzaxis">',
    '#suffix' => '</div>',
    '#default_value' => isset($defaults['zzaxis3'])?$defaults['zzaxis3']:'NULL',
  );

  $form['data']['yaxis4'] = array(
    '#title' => t('Value-4'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 25,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['yaxis4'])?$defaults['yaxis4']:'NULL',
  );

  $form['data']['zaxis4'] = array(
    '#title' => t('String Value-4'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 26,
    '#options' => $other_fields,
  );

  $form['data']['zzaxis4'] = array(
    '#title' => t('String Value-4-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 27,
    '#options' => $other_fields,
    '#prefix' => '<div class="zzaxis">',
    '#suffix' => '</div>',
    '#default_value' => isset($defaults['zzaxis4'])?$defaults['zzaxis4']:'NULL',
  );

  $form['data']['yaxis5'] = array(
    '#title' => t('Value-5'),
    '#description' => t('Must be number.'),
    '#type' => 'select',
    '#weight' => 28,
    '#options' => $numeric_fields,
    '#default_value' => isset($defaults['yaxis5'])?$defaults['yaxis5']:'NULL',
  );

  $form['data']['zaxis5'] = array(
    '#title' => t('String Value-5'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 29,
    '#options' => $other_fields,
    '#default_value' => isset($defaults['zaxis5'])?$defaults['zaxis5']:'NULL',
  );

  $form['data']['zzaxis5'] = array(
    '#title' => t('String Value-5-1'),
    '#description' => t('Must be string.'),
    '#type' => 'select',
    '#weight' => 30,
    '#options' => $other_fields,
    '#prefix' => '<div class="zzaxis">',
    '#suffix' => '</div>',
    '#default_value' => isset($defaults['zzaxis5'])?$defaults['zzaxis5']:'NULL',
  );
  //OPTIONS
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['options']['region'] = array(
    '#title' => t('Region'),
    '#description' => t('Select region'),
    '#type' => 'select',
    '#weight' => 31,
    '#options' => array('world' => t('World'), 'africa' => t('Africa'), 'asia' => t('Asia'), 'europe' => t('Europe'), 'middle_east' => t('Middle East'), 'south_america' => t('South America'), 'usa' => t('USA')),
    '#default_value' => isset($defaults['region'])?$defaults['region']:'world',
    '#prefix' => '<div class="map3">',
    '#suffix' => '</div>',
  );

  $form['options']['region1'] = array(
    '#title' => t('Region'),
    '#description' => t('Select region'),
    '#type' => 'select',
    '#weight' => 32,
    '#options' => array(
      'world' => t('World'),
      '005' => t('South America'),
      '013' => t('Central America'),
      '021' => t('North America'),
      '002' => t('All of Africa'),
      '017' => t('Central Africa'),
      '015' => t('Northern Africa'),
      '018' => t('Southern Africa'),
      '030' => t('Eastern Asia'),
      '034' => t('Southern Asia'),
      '035' => t('Asia/Pacific region'),
      '143' => t('Central Asia'),
      '145' => t('Middle East'),
      '151' => t('Northern Asia'),
      '154' => t('Northern Europe'),
      '155' => t('Western Europe'),
      '039' => t('Southern Europe'),
      'AF' => t('Afghanistan'),
      'AX' => t('Aland Islands'),
      'AL' => t('Albania'),
      'DZ' => t('Algeria'),
      'AS' => t('American Samoa'),
      'AD' => t('Andorra'),
      'AO' => t('Angola'),
      'AI' => t('Anguilla'),
      'AQ' => t('Antarctica'),
      'AG' => t('Antigua and Barbuda'),
      'AR' => t('Argentina'),
      'AM' => t('Armenia'),
      'AW' => t('Aruba'),
      'AU' => t('Australia'),
      'AT' => t('Austria'),
      'AZ' => t('Azerbaijan'),
      'BS' => t('Bahamas'),
      'BH' => t('Bahrain'),
      'BD' => t('Bangladesh'),
      'BB' => t('Barbados'),
      'BY' => t('Belarus'),
      'BE' => t('Belgium'),
      'BZ' => t('Belize'),
      'BJ' => t('Benin'),
      'BM' => t('Bermuda'),
      'BT' => t('Bhutan'),
      'BO' => t('Bolivia'),
      'BA' => t('Bosnia and Herzegovina'),
      'BW' => t('Botswana'),
      'BV' => t('Bouvet Island'),
      'BR' => t('Brazil'),
      'IO' => t('British Indian Ocean Territory'),
      'BN' => t('Brunei Darussalam'),
      'BG' => t('Bulgaria'),
      'BF' => t('Burkina Faso'),
      'BI' => t('Burundi'),
      'KH' => t('Cambodia'),
      'CM' => t('Cameroon'),
      'CA' => t('Canada'),
      'CV' => t('Cape Verde'),
      'KY' => t('Cayman Islands'),
      'CF' => t('Central African Republic'),
      'TD' => t('Chad'),
      'CL' => t('Chile'),
      'CN' => t('China'),
      'CX' => t('Christmas Island'),
      'CC' => t('Cocos (Keeling) Islands'),
      'CO' => t('Colombia'),
      'KM' => t('Comoros'),
      'CG' => t('Congo'),
      'CD' => t('DR Congo'),
      'CK' => t('Cook Islands'),
      'CR' => t('Costa Rica'),
      'CI' => t('Cote dIvoire'),
      'HR' => t('Croatia'),
      'CU' => t('Cuba'),
      'CY' => t('Cyprus'),
      'CZ' => t('Czech Republic'),
      'DK' => t('Denmark'),
      'DJ' => t('Djibouti'),
      'DM' => t('Dominica'),
      'DO' => t('Dominican Republic'),
      'EC' => t('Ecuador'),
      'EG' => t('Egypt'),
      'SV' => t('El Salvador'),
      'GQ' => t('Equatorial Guinea'),
      'ER' => t('Eritrea'),
      'EE' => t('Estonia'),
      'ET' => t('Ethiopia'),
      'FK' => t('Falkland Islands'),
      'FO' => t('Faroe Islands'),
      'FJ' => t('Fiji'),
      'FI' => t('Finland'),
      'FR' => t('France'),
      'GF' => t('French Guiana'),
      'PF' => t('French Polynesia'),
      'TF' => t('French Southern Territories'),
      'GA' => t('Gabon'),
      'GM' => t('Gambia'),
      'GE' => t('Georgia'),
      'DE' => t('Germany'),
      'GH' => t('Ghana'),
      'GI' => t('Gibraltar'),
      'GR' => t('Greece'),
      'GL' => t('Greenland'),
      'GD' => t('Grenada'),
      'GP' => t('Guadeloupe'),
      'GU' => t('Guam'),
      'GT' => t('Guatemala'),
      'GG' => t('Guernsey'),
      'GN' => t('Guinea'),
      'GW' => t('Guinea-Bissau'),
      'GY' => t('Guyana'),
      'HT' => t('Haiti'),
      'HM' => t('Heard Island and McDonald Islands'),
      'VA' => t('Holy See (Vatican City State)'),
      'HN' => t('Honduras'),
      'HK' => t('Hong Kong'),
      'HU' => t('Hungary'),
      'IS' => t('Iceland'),
      'IN' => t('India'),
      'ID' => t('Indonesia'),
      'IR' => t('Iran, Islamic Republic of'),
      'IQ' => t('Iraq'),
      'IE' => t('Ireland'),
      'IM' => t('Isle of Man'),
      'IL' => t('Israel'),
      'IT' => t('Italy'),
      'JM' => t('Jamaica'),
      'JP' => t('Japan'),
      'JE' => t('Jersey'),
      'JO' => t('Jordan'),
      'KZ' => t('Kazakhstan'),
      'KE' => t('Kenya'),
      'KI' => t('Kiribati'),
      'KP' => t('N Korea'),
      'KR' => t('S Korea'),
      'KW' => t('Kuwait'),
      'KG' => t('Kyrgyzstan'),
      'LA' => t('Lao Peoples Democratic Republic'),
      'LV' => t('Latvia'),
      'LB' => t('Lebanon'),
      'LS' => t('Lesotho'),
      'LR' => t('Liberia'),
      'LY' => t('Libyan Arab Jamahiriya'),
      'LI' => t('Liechtenstein'),
      'LT' => t('Lithuania'),
      'LU' => t('Luxembourg'),
      'MO' => t('Macao'),
      'MK' => t('Macedonia'),
      'MG' => t('Madagascar'),
      'MW' => t('Malawi'),
      'MY' => t('Malaysia'),
      'MV' => t('Maldives'),
      'ML' => t('Mali'),
      'MT' => t('Malta'),
      'MH' => t('Marshall Islands'),
      'MQ' => t('Martinique'),
      'MR' => t('Mauritania'),
      'MU' => t('Mauritius'),
      'YT' => t('Mayotte'),
      'MX' => t('Mexico'),
      'FM' => t('Micronesia'),
      'MD' => t('Moldova'),
      'MC' => t('Monaco'),
      'MN' => t('Mongolia'),
      'ME' => t('Montenegro'),
      'MS' => t('Montserrat'),
      'MA' => t('Morocco'),
      'MZ' => t('Mozambique'),
      'MM' => t('Myanmar'),
      'NA' => t('Namibia'),
      'NR' => t('Nauru'),
      'NP' => t('Nepal'),
      'NL' => t('Netherlands'),
      'AN' => t('Netherlands Antilles'),
      'NC' => t('New Caledonia'),
      'NZ' => t('New Zealand'),
      'NI' => t('Nicaragua'),
      'NE' => t('Niger'),
      'NG' => t('Nigeria'),
      'NU' => t('Niue'),
      'NF' => t('Norfolk Island'),
      'MP' => t('Northern Mariana Islands'),
      'NO' => t('Norway'),
      'OM' => t('Oman'),
      'PK' => t('Pakistan'),
      'PW' => t('Palau'),
      'PS' => t('Palestinian Territory, Occupied'),
      'PA' => t('Panama'),
      'PG' => t('Papua New Guinea'),
      'PY' => t('Paraguay'),
      'PE' => t('Peru'),
      'PH' => t('Philippines'),
      'PN' => t('Pitcairn'),
      'PL' => t('Poland'),
      'PT' => t('Portugal'),
      'PR' => t('Puerto Rico'),
      'QA' => t('Qatar'),
      'RE' => t('Reunion'),
      'RO' => t('Romania'),
      'RU' => t('Russian Federation'),
      'RW' => t('Rwanda'),
      'BL' => t('Saint Barthelemy'),
      'SH' => t('Saint Helena'),
      'KN' => t('Saint Kitts and Nevis'),
      'LC' => t('Saint Lucia'),
      'MF' => t('Saint Martin'),
      'PM' => t('Saint Pierre and Miquelon'),
      'VC' => t('Saint Vincent and the Grenadines'),
      'WS' => t('Samoa'),
      'SM' => t('San Marino'),
      'ST' => t('Sao Tome and Principe'),
      'SA' => t('Saudi Arabia'),
      'SN' => t('Senegal'),
      'RS' => t('Serbia'),
      'SC' => t('Seychelles'),
      'SL' => t('Sierra Leone'),
      'SG' => t('Singapore'),
      'SK' => t('Slovakia'),
      'SI' => t('Slovenia'),
      'SB' => t('Solomon Islands'),
      'SO' => t('Somalia'),
      'ZA' => t('South Africa'),
      'GS' => t('South Georgia and the South Sandwich Islands'),
      'ES' => t('Spain'),
      'LK' => t('Sri Lanka'),
      'SD' => t('Sudan'),
      'SR' => t('Suriname'),
      'SJ' => t('Svalbard and Jan Mayen'),
      'SZ' => t('Swaziland'),
      'SE' => t('Sweden'),
      'CH' => t('Switzerland'),
      'SY' => t('Syrian Arab Republic'),
      'TW' => t('Taiwan'),
      'TJ' => t('Tajikistan'),
      'TZ' => t('Tanzania'),
      'TH' => t('Thailand'),
      'TL' => t('Timor-Leste'),
      'TG' => t('Togo'),
      'TK' => t('Tokelau'),
      'TO' => t('Tonga'),
      'TT' => t('Trinidad and Tobago'),
      'TN' => t('Tunisia'),
      'TR' => t('Turkey'),
      'TM' => t('Turkmenistan'),
      'TC' => t('Turks and Caicos Islands'),
      'TV' => t('Tuvalu'),
      'UG' => t('Uganda'),
      'UA' => t('Ukraine'),
      'AE' => t('United Arab Emirates'),
      'GB' => t('United Kingdom'),
      'US' => t('United States'),
      'UM' => t('United States Minor Outlying Islands'),
      'UY' => t('Uruguay'),
      'UZ' => t('Uzbekistan'),
      'VU' => t('Vanuatu'),
      'VE' => t('Venezuela'),
      'VN' => t('Viet Nam'),
      'VG' => t('Virgin Islands, British'),
      'VI' => t('Virgin Islands'),
      'WF' => t('Wallis and Futuna'),
      'EH' => t('Western Sahara'),
      'YE' => t('Yemen'),
      'ZM' => t('Zambia'),
      'ZW' => t('Zimbabwe'),
    ),
    '#default_value' => isset($defaults['region1'])?$defaults['region1']:'world',
    '#prefix' => '<div class="map1">',
    '#suffix' => '</div>',
  );

  $form['options']['is3D'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is 3D'),
    '#weight' => 33,
    '#default_value' => isset($defaults['is3D'])?$defaults['is3D']:1,
  );

  $form['options']['is_stacked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Stacked'),
    '#return_value' => 1,
    '#weight' => 34,
    '#default_value' => isset($defaults['is_stacked'])?$defaults['is_stacked']:0,
  );

  $form['options']['enableTooltip'] = array(
    '#type' => 'checkbox',
    '#weight' => 52,
    '#title' => t('Enable Tooltip'),
    '#return_value' => 1,
    '#prefix' => '<div class="labels">',
    '#suffix' => '</div>',
  );

  $form['options']['linlog'] = array(
    '#title' => t('Scale'),
    '#type' => 'select',
    '#weight' => 35,
    '#options' => array(
      'lin' => 'Lin',
      'log' => 'Log',
    ),
    '#default_value' => isset($defaults['linlog'])?$defaults['linlog']:'lin',
  );

  $form['options']['color'] = array(
    '#title' => t('Colors'),
    '#description' => t('Color palette.'),
    '#type' => 'select',
    '#options' => array(
      '0' => t('Default'),
      '1' => t('City Funky'),
      '2' => t('Pure Classic'),
      '3' => t('Ecology green'),
      '4' => t('Clear blue'),
      '5' => t('Lavender'),
      '6' => t('Burgundy red'),
      '7' => t('Orange Sun'),
      '8' => t('Vidi'),
      '9' => t('Logic'),
      '10' => t('Vivid'),
      '11' => t('Max contrast'),
      '12' => t('Web2 Bold'),
      '13' => t('Web2 Muted'),
      '14' => t('Neutral'),
      '15' => t('Map blue'),
      '16' => t('Map red'),
      '17' => t('Map violet'),
      '18' => t('Map kivi'),
      '19' => t('Orange-scaled'),
      '20' => t('Green-scaled'),
    ),
    '#default_value' => isset($defaults['color'])?$defaults['color']:'0',
  );

  $form['options']['legend'] = array(
    '#title' => t('Legend position'),
    '#type' => 'select',
    '#weight' => 37,
    '#options' => array(
      'right' => t('Right'),
      'left' => t('Left'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
      'none' => t('None'),
    ),
    '#default_value' => isset($defaults['legend'])?$defaults['legend']:'right',
  );

  $form['options']['legendBackgroundColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['legendFontSize'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['legendTextColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['backgroundColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['borderColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['titleColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['titleFontSize'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['axisColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['axisBackgroundColor'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['axisFontSize'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['titleX'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['titleY'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['pieJoinAngle'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['pieMinimalAngle'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['showChartButtons'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showHeader'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showSelectListComponent'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showSidePanel'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showXMetricPicker'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showYMetricPicker'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showXScalePicker'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showYScalePicker'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showAdvancedPanel'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['state'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['displayAnnotations'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['displayAnnotationsFilter'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['allowHtml'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['displayDateBarSeparator'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['displayExactValues'] = array(
    '#type' => 'hidden',
    '#value' => 0,
  );
  $form['options']['displayLegendDots'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['displayRangeSelector'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  $form['options']['displayZoomButtons'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['enableScrollWheel'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showTipMap'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['options']['showLineMap'] = array(
    '#type' => 'hidden',
    '#value' => 0,
  );

  $form['options']['lineColorMap'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['lineWidthMap'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['zoomLevelMap'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['options']['GaugeMin'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['GaugeMax'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['greenFrom'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['greenTo'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['yellowFrom'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['yellowTo'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['redFrom'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['redTo'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['options']['emlink'] = array(
    '#type' => 'hidden',
    '#value' => '0',
  );
  $form['options']['emlinkforever'] = array(
    '#type' => 'hidden',
    '#value' => '1',
  );

  if (user_access('administer views')) {

    $form['previewbt'] = array(
      '#weight' => 100,
      '#type' => 'button',
      '#value' => t('Preview new'),
      '#attributes' => array('onclick' => 'return false;'),
    );


    $form['submit'] = array(
      '#weight' => 101,
      '#type' => 'submit',
      '#value' => t('Save>>'),
      //'#prefix' => '<INPUT id="previewbt" type="button" value="Preview"/>'
    );
  }

  return $form;
}

function vidi_gvs_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    if (user_access('administer views')) {
      $build_id = $form_state['values']['form_build_id'];
      $_SESSION["$build_id"] = $form;
      unset($form_state['storage']);
      $form_state['redirect'] = array('vidi/view_generation', "build_id=$build_id&source=gvs");
    }
  }
  else {
    return;
  }
}