<?php

if (!module_exists('gvs') && !module_exists('tagmap') && !module_exists('timelinemap')) {
  drupal_set_message(t("If you installed any visualization module, you could select one type"), 'warning');

  //t('To use tagmap visualization, please download the tagmap module from <a href="@tagmap-module-download">here</a>', array('@tagmap-module-download' => 'http://drupal.org/project/tagmap'))
  echo t('No visualizations available, please install:
        <ul>
          <li><a href ="@tagmap-module-download">drupal.org/project/tagmap</a></li>
          <li><a href ="@gvs-module-download">drupal.org/project/gvs</a></li>
          <li><a href ="@timelinemap-module-download">drupal.org/project/timelinemap</a></li>
        </ul>', array('@timelinemap-module-download' => 'http://drupal.org/project/timelinemap', '@gvs-module-download' => 'http://drupal.org/project/gvs', '@tagmap-module-download' => 'http://drupal.org/project/tagmap'));
  return;
}

drupal_add_css(drupal_get_path('module', 'vidi') . '/css/chose_type.css');
drupal_add_js(drupal_get_path('module', 'vidi') . '/js/chose_type.js');
drupal_add_js(array('imagePath' => base_path() . drupal_get_path('module', 'vidi') . '/images/'), 'setting');
?>

<div id="vidi_navcontainer">
  <ul>
    <li><a href="#time_icons" onclick="return false;" ><img id="time_img" class="vidi_nav_img" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/time_data_tab.jpg" width="210" height="42" alt="<?php echo t('Time data'); ?>"></a></li>
    <li><a href="#geo_icons" onclick="return false;" ><img id="geo_img" class="vidi_nav_img" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/geo_data_tab.jpg" width="210" height="42" alt="<?php echo t('Geo data'); ?>"></a></li>
    <li><a href="#classic_icons" onclick="return false;" ><img id="classic_img" class="vidi_nav_img" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/classic_data_tab.jpg" width="210" height="42" alt="<?php echo t('Comparation data'); ?>"></a></li>
  </ul>


  <div id="icon_boxes_wrapper">
    <div id="icon_boxes">
      <div id="time_icons" class="vidi_icons">
        <?php if (module_exists('gvs')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Motion chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/motion_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>9)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>9)));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A dynamic chart to explore several indicators over time. The chart is rendered within the browser using Flash.<br/>
              <strong>Data Format</strong><br/>
              - The first column must be of type "string" and contain the entity names
              (e.g., "Apples", "Oranges".., in the example).
            '); ?>
            <br/>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <div class="detailed_description">
              <p>
                <?php echo t('
                  - The second column must contain time values. Time can be expressed in any of the following formats:<br/>
                  <span class="description_item">Year - Column type: "number". Example: 2008.</span>
                  <span class="description_item">Month, day and year - Column type: "date"; values should be javascript Date instances.</span>
                  <span class="description_item">Week number- Column type: "string"; values should use the pattern YYYYWww,
                  which conforms to ISO 8601. Example: "2008W03".</span>
                  <span class="description_item">Quarter - Column type: "string"; the values should have the pattern YYYYQq, which conforms to ISO 8601. Example: "2008Q3".</span>
                '); ?>
              </p>
              <p>
                <?php echo t('
                  <span class="description_item">- Subsequent columns can be of type "number" or "string". Number columns will
                  show up in the dropdown menus for X, Y, Color and Size axes.</span>
                  <span class="description_item">String columns will only appear in the dropdown menu for Color.</span>
                '); ?>
              </p>
                <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_motion_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/MotionChart.csv", array('html'=>TRUE)); ?>
                <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/MotionChart.csv", array('html'=>TRUE)); ?>
            </div>
          </div>
        </div>
        <?php endif; ?>


        <?php if (module_exists('timelinemap')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Timeline map'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/timelinemap_02.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'basic')));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'basic')));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              Timeline map allows you to load one or more datasets onto both a map and a timeline simultaneously. Items in the visible range of the timeline are displayed on the Google map as markers. Data can be imported into Drupal database externally or Drupal nodes can be used.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- a column that contains the geographic latitude (decimal number)</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- a column that contains the geographic longitude (decimal number)</span>
                <span class="description_item">- a column that contains the start date (date)</span>
                <span class="description_item">- a column that contains the end date (just for basic timelinemap type) (date)</span>
                <span class="description_item">- a column that contains description which can be displayed in the info cloud, this field isn´t required (text)</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_timelinemap_bacis_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap-basic.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap-basic.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Timeline map with path'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/timelinemap_01.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'pathline')));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'pathline')));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A variant of the Timeline map enabling drawing paths between markers on the map.<br/>
              <strong>Data Format</strong><br/>
              Datasets must contain the following columns:<br/>
              <span class="description_item">- a column that contains the geographic latitude (decimal number)</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- a column that contains the geographic longitude (decimal number)</span>
                <span class="description_item">- a column that contains the start date (date)</span>
                <span class="description_item">- (optional) a column that contains description which can be displayed in the info cloud (text)</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_timelinemap_path_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>

        <?php if (module_exists('gvs')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Annotated timeline'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/annotated_timeline.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>10)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>10)));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              An interactive time series line chart with optional annotations. The chart is rendered within the browser using Flash.<br />
              <strong>Data Format</strong><br/>
              You can display one or more lines on your chart. Each row represents an X position on the chart--that is, a specific time; each line is described by a set of one to three columns.<br />
              <span class="description_item">- The first column is of type date or datetime, and specifies the X value of the point on the chart.
              If this column is of type date (and not datetime) then the smallest time resolution on the X axis will be one day.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                - Each data line is then described by a set of one to three additional columns as described here:
                <span class="description_item">- Y value - [Required, Number] The first column in each set describes the value of the line at
                  the corresponding time from the first column. The column label is displayed on the chart as
                  the title of that line.</span>
                <span class="description_item">- Annotation title - [Optional, String] If a string column follows the value column, and the
                  displayAnnotations option is TRUE, this column holds a short title describing this point.</span>
                For instance, if this line represents temperature in Brazil, and this point is a very high
                number, the title could be "Hottest day on record".
                <span class="description_item">- Annotation text - [Optional string] If a second string column exists for this series, the
                  cell value will be used as additional descriptive text for this point. You must set the option
                  displayAnnotations to TRUE to use this column. You can use HTML tags, if you set allowHtml to TRUE;
                  there is essentially no size limit, but note that excessively long entries might overflow the
                  display section. You are not required to have this column even if you have an annotation title
                  column for this point.</span>
                <span class="description_item">The column label is not used by the chart. For example, if this were the
                  hottest day on record point, you might say something like "Next closest day was 10 degrees cooler!".</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_annotated_timeline_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/ATL.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/ATL.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>
      </div><!--//time_icons-->


      <div id="geo_icons" class="vidi_icons">
        <?php if (module_exists('tagmap')): ?>
        <div class="vidi_type clear-block">
          <h2> <?php echo t('Tag map'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tag_map.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'tagmap_form')));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'tagmap_form')));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('Displays weighted tags accross a Google map. <br/>
              <strong>Data Format</strong><br/>
              Datasets must contain the following columns:<br/>
              <span class="description_item">- a column that contains the geographic latitude (decimal number)</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- a column that contains the geographic longitude (decimal number)</span>
                <span class="description_item">- a column that contains tags (the string that is actually rendered over the map)</span>
                <span class="description_item">- a column that contains numerical value that represents the weight of the corresponding tag</span>
                <span class="description_item">Besides these mandatory fields, your data can contain any number of columns that can be displayed in the information cloud that is shown when you click some tag on the map. You can choose which fields will be displayed in the info cloud, as well as their order. You can also choose one field that will be rendered as the title of the info cloud.</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_tagmap_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Tagmap.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Tagmap.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>


        <?php if (module_exists('gvs')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Intensity map'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/intensity_map.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>8)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>8)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              Intensity map that highlights regions or countries based on relative values.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- The first column should be a string, and contain country ISO codes or USA state codes.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- Any number of columns can follow, all must be numeric. Each column is displayed as a separate map tab.</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_intensity_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/IntMap.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/IntMap.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Geomap'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/geomap.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>7)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>7)));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A map of a country, continent, or region map, with colors and values assigned to specific regions. Values are displayed as a color scale, and you can specify optional hovertext for regions. The map is rendered in the browser using an embedded Flash player. It is not scrollable or draggable, but can be configured to allow zooming.<br/>
              <strong>Data Format</strong><br/>
              Two address formats are supported, each of which supports a different number of columns, and different data types for each column. All addresses in the table must use one or the other; you cannot mix types:<br/>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item"><strong>Format 1: Latitude/Longitude locations.</strong> <br />
                  This format works only when the dataMode option is "markers".
                  If this format is used, you do not need to include the Google Map Javascript. <br />
                  The location is entered in two columns, plus two optional columns:
                  <br />
                  - [Number, Required] A latitude.
                  Positive numbers are north, negative numbers are south.
                  <br />
                  - [Number, Required] A longitude.
                  Positive numbers are east, negative numbers are west.
                  <br />
                  - [Number, Optional] A numeric value displayed when the user hovers over this region.
                  If column 4 is used, this column is required.
                  <br />
                  - [String, Optional] Additional string text displayed when the user hovers over this region.</span>
                <br />
                <span class="description_item"><strong>Format 2: Address, country name, region name locations, or US metropolitan area codes.</strong> <br />
                  This format works with the dataMode option set to either "markers" or "regions".
                  The location is entered in one column, plus two optional columns:<br />
                  - [String, Required] A map location. The following formats are accepted:
                  <br />
                  - A specific address (for example, "1600 Pennsylvania Ave").
                  <br />
                  - A country name as a string (for example, "England"),
                  or an uppercase ISO-3166 code or its English text
                  equivalent (for example, "GB" or "United Kingdom").
                  <br />
                  - An uppercase ISO-3166-2 region code name or its English text equivalent
                  (for example, "US-NJ" or "New Jersey"). Note: Regions can only be specified
                  when the dataMode option is set to "regions".
                  <br />
                  - A metropolitan area code. These are three-digit metro codes used to
                  designate various regions; US codes only supported.
                  Note that these are not the same as telephone area codes.<br />
                  Two optional columns:<br />
                </span><span class="description_item">- [Number, Optional] A numeric value displayed when the user hovers over this region.
                  If column 3 is used, this column is required.
                  <br />
                  - [String, Optional] Additional string text displayed when the user hovers over this region.</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_geomap_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/GeoMap.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/GeoMap.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>


        <?php if (module_exists('timelinemap')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Timeline map'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/timelinemap_02.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'basic')));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'basic')));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              Timeline map allows you to load one or more datasets onto both a map and a timeline simultaneously. Items in the visible range of the timeline are displayed on the Google map as markers. Data can be imported into Drupal database externally or Drupal nodes can be used.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- a column that contains the geographic latitude (decimal number)</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- a column that contains the geographic longitude (decimal number)</span>
                <span class="description_item">- a column that contains the start date (date)</span>
                <span class="description_item">- a column that contains the end date (just for basic timelinemap type) (date)</span>
              '); ?>
              <span class="description_item">- a column that contains description which can be displayed in the info cloud, this field isn't required (text)</span>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_timelinemap_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap-basic.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap-basic.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>


        <div class="vidi_type clear-block">
          <h2><?php echo t('Timeline map with path'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/timelinemap_01.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'pathline')));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'timelinemap_form', 'datasource'=>'pathline')));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A variant of the Timeline map enabling drawing paths between markers on the map.<br/>
              <strong>Data Format</strong><br/>
              Datasets must contain the following columns:<br/>
              <span class="description_item">- a column that contains the geographic latitude (decimal number)</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- a column that contains the geographic longitude (decimal number)</span>
                <span class="description_item">- a column that contains the start date (date)</span>
                <span class="description_item">- (optional) a column that contains description which can be displayed in the info cloud (text)</span>
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_timelinemap_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Timelinemap.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>

      </div><!--//geo_icons-->


      <div id="classic_icons" class="vidi_icons">
        <?php if (module_exists('gvs')): ?>
        <div class="vidi_type clear-block">
          <h2><?php echo t('Pie chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/pie_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>0)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>0)));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A pie chart that is rendered within the browser using SVG or VML. Displays tooltips when hovering over slices.<br/>
              <strong>Data Format</strong><br/>
              Two columns:<br/>
              <span class="description_item">- The first column should be a string, and contain the slice label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('<span class="description_item">The second column should be a number, and contain the slice value.</span>'); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_pie_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Pie.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Pie.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Gauge chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/gauge_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>1)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>1)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              One or more gauges are rendered within the browser using SVG or VML. Each numeric value is displayed as a gauge.<br/>
              <strong>Data Format</strong><br/>
              Two columns:<br/>
              <span class="description_item">- The first column should be a string, and contain the gauge label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <span class="description_item"><?php echo t('- The second column should be a number, and contain the gauge value.'); ?></span>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_gauge_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Pie.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Pie.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Bar chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/bar_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>2)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>2)));
            ?>
          </div>

          <div class="vidi_type_description">
            <?php echo t('
              A horizontal bar chart that is rendered within the browser using SVG or VML. Displays tips when hovering over bars.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- The first column should be a string, and contain the category label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <span class="description_item"<?php echo t('- >Any number of columns can follow, all must be numeric.'); ?></span>
                <?php echo t('Each column is displayed as a separate line.'); ?>
                <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_bar_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
                <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Area chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/area_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>3)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>3)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              An area chart that is rendered within the browser using SVG or VML. Displays tips when hovering over points.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- The first column should be a string, and contain the category label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- Any number of columns can follow, all must be numeric.</span>
                Each column is displayed as a separate line.
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_area_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Column chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/column_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>4)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>4)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              A vertical bar chart that is rendered within the browser using SVG or VML. Displays tips when hovering over bars.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- The first column should be a string, and contain the category label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- Any number of columns can follow, all must be numeric.</span>
                Each column is displayed as a separate line.
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_bar_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>


        <div class="vidi_type clear-block">
          <h2><?php echo t('Line chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/line_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>5)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>5)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              A line chart that is rendered within the browser using SVG or VML. Displays tips when hovering over points.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- The first column should be a string, and contain the category label.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- Any number of columns can follow, all must be numeric.</span>
                Each column is displayed as a separate line.
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_line_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/AreaBarColLine.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>

        <div class="vidi_type clear-block">
          <h2><?php echo t('Scatter chart'); ?></h2>
          <div class="vidi_type_image">
            <?php
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/scatter_chart.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>6)));
              echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/choose_data.jpg\" >", "vidi/visualizations_links", array('html'=>TRUE, 'query'=>array('return_link'=>'gvs_form', 'datasource'=>6)));
            ?>
          </div>
          <div class="vidi_type_description">
            <?php echo t('
              A scatter chart that is rendered within the browser using SVG or VML. Displays tips when hovering over points. A scatter chart is used to map correlation between sets of numbers.<br/>
              <strong>Data Format</strong><br/>
              <span class="description_item">- Two or more columns are required, all must be numeric.</span>
            '); ?>
            <a href="#" class="show_more"><img alt="Read more" src="<?php print base_path() . drupal_get_path('module', 'vidi'); ?>/images/read_more.jpg "></a>
            <p class="detailed_description">
              <?php echo t('
                <span class="description_item">- The values in the first column are used for the X-axis.</span>
                <span class="description_item">- The values in following columns are used for the Y-axis.</span>
                Each column is displayed with a separate color.
              '); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/tbl_scatter_chart_table.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Scatter.csv", array('html'=>TRUE)); ?>
              <?php echo l("<img src=\"".  base_path() . drupal_get_path('module', 'vidi') . "/images/download.jpg\" >", $base_url . drupal_get_path('module', 'vidi') . "/csvexamples/Scatter.csv", array('html'=>TRUE)); ?>
            </p>
          </div>
        </div>
        <?php endif; ?>
      </div><!--//classic_icons-->
    </div>
  </div>

</div>