
Drupal.behaviors.vidi = function(context) {

  active_tab = "#time_img"; //sets initially active tab to #time_img
  imagePath = Drupal.settings.imagePath;

  $('img', '.vidi_type_image').css("border-bottom", "1px solid white");

  // hide all icons
  $('#time_icons').hide();
  $('#geo_icons').hide();
  $('#classic_icons').hide();

  $("#geo_img").css("border", "1px solid #868686");
  $("#classic_img").css("border", "1px solid #868686");
  $("#time_img").css("border", "1px solid #868686");

  
  curentUrl = document.location.toString();
  curentAnchor = getAnchorName(curentUrl);

  if((curentAnchor == '#time_icons') || (curentAnchor == '#geo_icons') || (curentAnchor == '#classic_icons')){
    APrefix = curentAnchor.replace("_icons", "");
    APrefix = APrefix.replace("#", "");
    var show_me = curentAnchor + "";
    show_me = APrefix + "_img";
    active_tab = "#" + show_me;
  }else{//
    APrefix = "time";//initially active tab
  }
  showClickedAndHideOtherIcons('#time_icons', active_tab);
  $(active_tab).attr("src", imagePath + APrefix + "_data_tab_active" + ".jpg");

  //hide detailed descriptions
  $('.detailed_description').hide();

  $(".show_more").click(function () {
    if (!$(this).parent().children(".detailed_description").is(":visible")) {
      $(this).parent().children(".detailed_description").show('20');
      $("img", this).attr("src", imagePath + "hide.jpg");
      $("img", this).attr("alt", "Hide");

    }else{
      $(this).parent().children(".detailed_description").hide('20');
      $("img", this).attr("src", imagePath + "read_more.jpg");
      $("img", this).attr("alt", "Read more");

    }
    return false;

  });

  //currently not used
  $(".hide_details").click(function () {
    $(this).parent(".detailed_description").hide('20');
    return false;
  });

  function getAnchorName(myUrl){
    if (myUrl.match('#')) { // URL contains anchor
      return "#" + myUrl.split('#')[1];
    }
    else 
      return "";
  }

  // show/hide processes
  function showClickedAndHideOtherIcons(show_me, active_me) {
    // hide all icons
    $('#time_icons').hide('5');
    $('#geo_icons').hide('5');
    $('#classic_icons').hide('5');


    //set non active tabs images
    if (active_me != "#time_img") {
      $("#time_img").attr("src", imagePath + "time_data_tab.jpg");
      $("#time_img").css("border", "none");
      $("#time_img").css("border-bottom", "1px solid #868686");
    }
    if (active_me != "#geo_img") {
      $("#geo_img").attr("src", imagePath + "geo_data_tab.jpg");
      $("#geo_img").css("border", "none");
      $("#geo_img").css("border-bottom", "1px solid #868686");
    }
    if (active_me != "#classic_img") {
      $("#classic_img").attr("src", imagePath + "classic_data_tab.jpg");
      $("#classic_img").css("border", "none");
      $("#classic_img").css("border-bottom", "1px solid #868686");
    }

    //set active tab
    if (active_tab != active_me) {
      active_tab = active_me;
      $(active_me).css("border", "1px solid #868686");
    }

    // Show "show_me" icons only if not already shown
    if (!$(show_me).is(":visible")) {
      $(show_me).show('35');
      $(active_me).css("border-bottom", "1px solid white");
    }
  }


  //time_img events, hover and click
  $("#time_img").hover(
    function() {
      $("#time_img").attr("src", imagePath + "time_data_tab_active.jpg");
    },
    function() {
      if (active_tab != '#time_img') {
        $("#time_img").attr("src", imagePath + "time_data_tab.jpg");
      }
    });


  $("#time_img").click(function () {
    showClickedAndHideOtherIcons("#time_icons", "#time_img");
  });



  //geo_icons events, hover and click
  $("#geo_img").hover(
    function() {
      $("#geo_img").attr("src", imagePath + "geo_data_tab_active.jpg");
    },
    function() {
      if (active_tab != '#geo_img') {
        $("#geo_img").attr("src", imagePath + "geo_data_tab.jpg");
      }
    });


  $("#geo_img").click(function () {
    showClickedAndHideOtherIcons("#geo_icons", "#geo_img");
  });



  //classic_icons events, hover and click
  $("#classic_img").hover(
    function() {
      $("#classic_img").attr("src", imagePath + "classic_data_tab_active.jpg");
    },
    function() {
      if (active_tab != '#classic_img') {
        $("#classic_img").attr("src", imagePath + "classic_data_tab.jpg");
      }
    });


  $("#classic_img").click(function () {
    showClickedAndHideOtherIcons("#classic_icons", "#classic_img");
  });

};// /Drupal.behaviors.vidi = function(context) {