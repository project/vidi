
google.load("visualization", "1", {
  packages:["annotatedtimeline", "areachart", "barchart", "piechart", "columnchart", "linechart", "scatterchart", "imageareachart", "gauge", "motionchart", "geomap", "intensitymap", "map"]
});

function changefields(typeid) {
  switch (typeid) {
    case "0":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").hide();
      $("#edit-yaxis3-wrapper").hide();
      $("#edit-yaxis4-wrapper").hide();
      $("#edit-yaxis5-wrapper").hide();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $(".zzaxis").hide();

      $("#edit-mcdateformat-wrapper").hide();
      $("#edit-title-wrapper").show();
      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").show();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      break;
    case "1":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").hide();
      $("#edit-yaxis3-wrapper").hide();
      $("#edit-yaxis4-wrapper").hide();
      $("#edit-yaxis5-wrapper").hide();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $(".zzaxis").hide();

      $("#edit-mcdateformat-wrapper").show();
      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").show();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").hide();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "2":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").show();
      $("#edit-is3D-wrapper").show();
      $("#edit-linlog-wrapper").show();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "3":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").show();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").show();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "4":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").show();
      $("#edit-is3D-wrapper").show();
      $("#edit-linlog-wrapper").show();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "5":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $(".zzaxis").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").show();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "6":
      $("#edit-xaxis-wrapper").hide();
      $("#edit-xaxis1-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").show();
      $("#edit-legend-wrapper").show();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map3").hide();
      $(".map2").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "7":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").hide();
      $("#edit-yaxis3-wrapper").hide();
      $("#edit-yaxis4-wrapper").hide();
      $("#edit-yaxis5-wrapper").hide();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").hide();

      $(".map1").show();
      if ($("#edit-geomaptype").val()=="regions") {
        $("#edit-geomaplat-wrapper").hide();
        $("#edit-geomaplon-wrapper").hide();
      }
      else {
        $("#edit-geomapla-wrappert").show();
        $("#edit-geomaplon-wrapper").show();
      }

      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").show();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "8":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").show();
      $("#edit-maptype-wrapper").hide();
      $("#edit-geomapstring-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").hide();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "9":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").show();
      $("#edit-yaxis1-wrapper").hide();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").show();
      $("#edit-zaxis2-wrapper").show();
      $("#edit-zaxis3-wrapper").show();
      $("#edit-zaxis4-wrapper").show();
      $("#edit-zaxis5-wrapper").show();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").show();
      $(".labels").hide();
      $("#edit-color-wrapper").hide();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").show();
      break;
    case "10":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").hide();

      $("#edit-yaxis11-wrapper").show();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").show();
      $("#edit-yaxis3-wrapper").show();
      $("#edit-yaxis4-wrapper").show();
      $("#edit-yaxis5-wrapper").show();

      $("#edit-zaxis1-wrapper").show();
      $("#edit-zaxis2-wrapper").show();
      $("#edit-zaxis3-wrapper").show();
      $("#edit-zaxis4-wrapper").show();
      $("#edit-zaxis5-wrapper").show();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").hide();
      $(".map3").hide();
      $("#edit-geomapstring-wrapper").hide();
      $("#edit-maptype-wrapper").hide();

      $(".zzaxis").show();
      $(".atl").show();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").show();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
    case "11":
      $("#edit-xaxis1-wrapper").hide();
      $("#edit-xaxis-wrapper").show();

      $("#edit-yaxis11-wrapper").hide();
      $("#edit-yaxis1-wrapper").show();
      $("#edit-yaxis2-wrapper").hide();
      $("#edit-yaxis3-wrapper").hide();
      $("#edit-yaxis4-wrapper").hide();
      $("#edit-yaxis5-wrapper").hide();

      $("#edit-zaxis1-wrapper").hide();
      $("#edit-zaxis2-wrapper").hide();
      $("#edit-zaxis3-wrapper").hide();
      $("#edit-zaxis4-wrapper").hide();
      $("#edit-zaxis5-wrapper").hide();

      $("#edit-enableTooltip-wrapper").hide();
      $("#edit-is_stacked-wrapper").hide();
      $("#edit-is3D-wrapper").hide();
      $("#edit-linlog-wrapper").hide();
      $("#edit-legend-wrapper").hide();
      $(".gauge").hide();
      $(".map1").hide();
      $(".map2").show();
      $("#edit-geomapstring-wrapper").show();
      $("#edit-maptype-wrapper").show();

      $(".map3").hide();
      $(".zzaxis").hide();
      $(".atl").hide();
      $(".motion").hide();
      $(".labels").hide();
      $("#edit-color-wrapper").hide();
      $("#edit-title-wrapper").show();
      $("#edit-mcdateformat-wrapper").hide();
      break;
  };
}

function resend() {
  $.ajax({
    url: nekiurl,
    data: "table=tablename&type="+$("#edit-type").val()+"&title="+$("#edit-title").val()+"&xaxis1="+$("#edit-xaxis1").val()+"&xaxis="+$("#edit-xaxis").val()+"&yaxis11="+$("#edit-yaxis11").val()+"&yaxis1="+$("#edit-yaxis1").val()+"&yaxis2="+$("#edit-yaxis2").val()+"&yaxis3="+$("#edit-yaxis3").val()+"&yaxis4="+$("#edit-yaxis4").val()+"&yaxis5="+$("#edit-yaxis5").val()
    +"&zaxis2="+$("#edit-zaxis2").val()+"&zaxis3="+$("#edit-zaxis3").val()+"&zaxis4="+$("#edit-zaxis4").val()+"&zaxis5="+$("#edit-zaxis5").val()
    +"&zaxis1="+$("#edit-zaxis1").val()+"&zzaxis1="+$("#edit-zzaxis1").val()+"&zzaxis2="+$("#edit-zzaxis2").val()+"&zzaxis3="+$("#edit-zzaxis3").val()+"&zzaxis4="+$("#edit-zzaxis4").val()+"&zzaxis5="+$("#edit-zzaxis5").val()
    +"&geomaptype="+$("#edit-geomaptype").val()+"&geomaplat="+$("#edit-geomaplat").val()+"&geomaplon="+$("#edit-geomaplon").val()+"&geomaplat1="+$("#edit-geomaplat1").val()+"&geomaplon1="+$("#edit-geomaplon1").val()
    +"&geomapstring="+$("#edit-geomapstring").val()+"&maptype="+$("#edit-maptype").val()+"&is_stacked="+$("#edit-is_stacked").is(":checked")+"&is3D="+$("#edit-is3D:checkbox").is(":checked")+"&linlog="+$("#edit-linlog").val()+"&legend="+$("#edit-legend").val()
    +"&region1="+$("#edit-region1").val()+"&region="+$("#edit-region").val()+"&color="+$("#edit-color").val()+"&mcdateformat="+$("#edit-mcdateformat").val()+"&clickedembed=0&update=update_for_replacment&filename=filename_for_replacment",
    success: function(data) {
      $("#chart_div").show();
      $("#embedinstructons").show();
      $("#embedlink").show();
      $("#back-button").show();
      $("#addscrip").html(data);
      drawChart();
    }
  });
}
    

$(document).ready(function() {
  changefields (requiredviz);
  $("#chart_div").hide();
  $("#embedinstructons").hide();
  $("#embedlink").hide();
  $("#back-button").hide();

  $("#edit-type").change(function() {
    changefields ($(this).val());
  //resend();
  });

  $("#edit-geomaptype").change(function() {
    if ($(this).val()=="regions") {
      $("#edit-geomaplat-wrapper").hide();
      $("#edit-geomaplon-wrapper").hide();
    }
    else {
      $("#edit-geomaplat-wrapper").show();
      $("#edit-geomaplon-wrapper").show();
    }
  });

  $("#edit-previewbt").click(function() {
    resend();
    $("#embedlink").show();
    $(".embed-code").hide();
  });

  $("#embedlink").click(function() {
    $.ajax({
      url: nekiurl,
      data: "table=tablename&type="+$("#edit-type").val()+"&title="+$("#edit-title").val()+"&xaxis1="+$("#edit-xaxis1").val()+"&xaxis="+$("#edit-xaxis").val()+"&yaxis11="+$("#edit-yaxis11").val()+"&yaxis1="+$("#edit-yaxis1").val()+"&yaxis2="+$("#edit-yaxis2").val()+"&yaxis3="+$("#edit-yaxis3").val()+"&yaxis4="+$("#edit-yaxis4").val()+"&yaxis5="+$("#edit-yaxis5").val()
      +"&zaxis2="+$("#edit-zaxis2").val()+"&zaxis3="+$("#edit-zaxis3").val()+"&zaxis4="+$("#edit-zaxis4").val()+"&zaxis5="+$("#edit-zaxis5").val()
      +"&zaxis1="+$("#edit-zaxis1").val()+"&zzaxis1="+$("#edit-zzaxis1").val()+"&zzaxis2="+$("#edit-zzaxis2").val()+"&zzaxis3="+$("#edit-zzaxis3").val()+"&zzaxis4="+$("#edit-zzaxis4").val()+"&zzaxis5="+$("#edit-zzaxis5").val()
      +"&geomaptype="+$("#edit-geomaptype").val()+"&geomaplat="+$("#edit-geomaplat").val()+"&geomaplon="+$("#edit-geomaplon").val()+"&geomaplat1="+$("#edit-geomaplat1").val()+"&geomaplon1="+$("#edit-geomaplon1").val()
      +"&geomapstring="+$("#edit-geomapstring").val()+"&maptype="+$("#edit-maptype").val()+"&is_stacked="+$("#edit-is_stacked").is(":checked")+"&is3D="+$("#edit-is3D:checkbox").is(":checked")+"&linlog="+$("#edit-linlog").val()+"&legend="+$("#edit-legend").val()
      +"&region1="+$("#edit-region1").val()+"&region="+$("#edit-region").val()+"&color="+$("#edit-color").val()+"&mcdateformat="+$("#edit-mcdateformat").val()+"&clickedembed=1&update=update_for_replacment&filename=filename_for_replacment",
      success: function(data) {
        $("#embedinstructons").html(data);
        $("#embedlink").hide();
      }
    });
  });

  
});