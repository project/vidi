
Drupal.vidi = {
  tagmapFormChange: function(args) {
  var value = $('#vidi-tagmap-form select#edit-clustering').val();
  if (value == 'clusterer2') {
    $('#vidi-tagmap-form .vidi-tagmap-clusterer-fields').fadeIn('slow');
  }
  else{
    $('#vidi-tagmap-form .vidi-tagmap-clusterer-fields').hide('slow');
  }
  },
  
  tablePreview: function(tablename) {
      var path = Drupal.settings.basePath + 'vidi/table/preview';
    $.ajax( {
      type : "POST",
      url : path,
      data : "tablename=" + tablename,
      success : function(msg) {
        $('.vidi-preview-wrapper').hide('fast');
        $('.vidi-preview-wrapper').html(msg);
        $('.vidi-preview-wrapper').fadeIn('slow');
      },
      error : function(msg) {
        alert('Error!');
      }
    });      
    }
}

Drupal.behaviors.vidi = function(context) {
  $('.vidi-table-ajax-callback').click(function() {Drupal.vidi.tablePreview($(this).attr('id'));});
  
  Drupal.vidi.tagmapFormChange();
  $('#vidi-tagmap-form #edit-clustering').change(function() {Drupal.vidi.tagmapFormChange();});
}
