<?php

/**
 * @file
 * vidi_view.inc
 */

//Prepare 
function _vidi_gvs_style_options($data) {
  $tablename = $data['tablename']['#value'];
  if ($data['data']['xaxis']['#value']=="NULL") {
    $xaxis='';
  }
  else {
    $xaxis=$data['data']['xaxis']['#value'];
  }
  if ($data['data']['xaxis1']['#value']=="NULL") {
    $xaxis1='';
  }
  else {
    $xaxis1=$data['data']['xaxis1']['#value'];
  }
  if ($data['data']['yaxis11']['#value']=="NULL") {
    $yaxis11='';
  }
  else {
    $yaxis11=$data['data']['yaxis11']['#value'];
  }
  if ($data['data']['yaxis1']['#value']=="NULL") {
    $yaxis1='';
  }
  else {
    $yaxis1=$data['data']['yaxis1']['#value'];
  }
  if ($data['data']['zaxis1']['#value']=="NULL") {
    $zaxis1='';
  }
  else {
    $zaxis1=$data['data']['zaxis1']['#value'];
  }
  if ($data['data']['zzaxis1']['#value']=="NULL") {
    $zzaxis1='';
  }
  else {
    $zzaxis1=$data['data']['zzaxis1']['#value'];
  }
  if ($data['data']['yaxis2']['#value']=="NULL") {
    $yaxis2='';
  }
  else {
    $yaxis2=$data['data']['yaxis2']['#value'];
  }
  if ($data['data']['zaxis2']['#value']=="NULL") {
    $zaxis2='';
  }
  else {
    $zaxis2=$data['data']['zaxis2']['#value'];
  }
  if ($data['data']['zzaxis2']['#value']=="NULL") {
    $zzaxis2='';
  }
  else {
    $zzaxis2=$data['data']['zzaxis2']['#value'];
  }
  if ($data['data']['yaxis3']['#value']=="NULL") {
    $yaxis3='';
  }
  else {
    $yaxis3=$data['data']['yaxis3']['#value'];
  }
  if ($data['data']['zaxis3']['#value']=="NULL") {
    $zaxis3='';
  }
  else {
    $zaxis3=$data['data']['zaxis3']['#value'];
  }
  if ($data['data']['zzaxis3']['#value']=="NULL") {
    $zzaxis3='';
  }
  else {
    $zzaxis3=$data['data']['zzaxis3']['#value'];
  }
  if ($data['data']['yaxis4']['#value']=="NULL") {
    $yaxis4='';
  }
  else {
    $yaxis3=$data['data']['yaxis4']['#value'];
  }
  if ($data['data']['zaxis4']['#value']=="NULL") {
    $zaxis4='';
  }
  else {
    $zaxis3=$data['data']['zaxis4']['#value'];
  }
  if ($data['data']['zzaxis4']['#value']=="NULL") {
    $zzaxis4='';
  }
  else {
    $zzaxis3=$data['data']['zzaxis4']['#value'];
  }
  if ($data['data']['yaxis5']['#value']=="NULL") {
    $yaxis5='';
  }
  else {
    $yaxis3=$data['data']['yaxis5']['#value'];
  }
  if ($data['data']['zaxis5']['#value']=="NULL") {
    $zaxis5='';
  }
  else {
    $zaxis3=$data['data']['zaxis5']['#value'];
  }
  if ($data['data']['zzaxis5']['#value']=="NULL") {
    $zzaxis5='';
  }
  else {
    $zzaxis3=$data['data']['zzaxis5']['#value'];
  }

  if ($data['data']['geomaplat']['#value']=="NULL") {
    $geomaplat='';
  }
  else {
    $geomaplat=$data['data']['geomaplat']['#value'];
  }
  if ($data['data']['geomaplon']['#value']=="NULL") {
    $geomaplon='';
  }
  else {
    $geomaplon=$data['data']['geomaplon']['#value'];
  }
  if ($data['data']['geomaplat1']['#value']=="NULL") {
    $geomaplat1='';
  }
  else {
    $geomaplat1=$data['data']['geomaplat1']['#value'];
  }
  if ($data['data']['geomaplon1']['#value']=="NULL") {
    $geomaplon1='';
  }
  else {
    $geomaplon1=$data['data']['geomaplon1']['#value'];
  }
  if ($data['data']['geomapstring']['#value']=="NULL") {
    $geomapstring='';
  }
  else {
    $geomapstring=$data['data']['geomapstring']['#value'];
  }
  if ($data['options']['color']['#value']=="NULL") {
    $color='';
  }
  else {
    $color=$data['options']['color']['#value'];
  }


  $style_options = array(
    'title' => $data['data']['title']['#value'],
    'width' => $data['width']['#value'],
    'height' => $data['height']['#value'],
    'type' => $data['data']['type']['#value'],
    'geomaptype' => $data['data']['geomaptype']['#value'],
    'geomaplat' => $geomaplat,
    'geomaplon' => $geomaplon,
    'geomaplat1' => $geomaplat1,
    'geomaplon1' => $geomaplon1,
    'geomapstring' => $geomapstring,
    'maptype' => $data['data']['maptype']['#value'],
    'xaxis' => $xaxis,
    'xaxis1' => $xaxis1,
    'yaxis11' => $yaxis11,
    'yaxis1' => $yaxis1,
    'zaxis1' => $zaxis1,
    'zzaxis1' => $zzaxis1,
    'yaxis2' => $yaxis2,
    'zaxis2' => $zaxis2,
    'zzaxis2' => $zzaxis2,
    'yaxis3' => $yaxis3,
    'zaxis3' => $zaxis3,
    'zzaxis3' => $zzaxis3,
    'yaxis4' => $yaxis4,
    'zaxis4' => $zaxis4,
    'zzaxis4' => $zzaxis4,
    'yaxis5' => $yaxis5,
    'zaxis5' => $zaxis5,
    'zzaxis5' => $zzaxis5,
    'region' => $data['options']['region']['#value'],
    'region1' => $data['options']['region1']['#value'],
    'is3D' => $data['options']['is3D']['#value'],
    'is_stacked' => $data['options']['is_stacked']['#value'],
    'linlog' => $data['options']['linlog']['#value'],
    'color' => $color,
    'legend' => $data['options']['legend']['#value'],
    'legendBackgroundColor' => $data['options']['legendBackgroundColor']['#value'],
    'legendFontSize' => $data['options']['legendFontSize']['#value'],
    'legendTextColor' => $data['options']['legendTextColor']['#value'],
    'backgroundColor' => $data['options']['backgroundColor']['#value'],
    'borderColor' => $data['options']['borderColor']['#value'],
    'titleColor' => $data['options']['titleColor']['#value'],
    'titleFontSize' => $data['options']['titleFontSize']['#value'],
    'axisColor' => $data['options']['axisColor']['#value'],
    'axisBackgroundColor' => $data['options']['axisBackgroundColor']['#value'],
    'axisFontSize' => $data['options']['axisFontSize']['#value'],
    'titleX' => $data['options']['titleX']['#value'],
    'titleY' => $data['options']['titleY']['#value'],
    'pieJoinAngle' => $data['options']['pieJoinAngle']['#value'],
    'pieMinimalAngle' => $data['options']['pieMinimalAngle']['#value'],
    'enableTooltip' => $data['options']['enableTooltip']['#value'],
    'mcdateformat' => $data['data']['mcdateformat']['#value'],
    'showChartButtons' => $data['options']['showChartButtons']['#value'],
    'showHeader' => $data['options']['showHeader']['#value'],
    'showSelectListComponent' => $data['options']['showSelectListComponent']['#value'],
    'showSidePanel' => $data['options']['showSidePanel']['#value'],
    'showXMetricPicker' => $data['options']['showXMetricPicker']['#value'],
    'showYMetricPicker' => $data['options']['showYMetricPicker']['#value'],
    'showXScalePicker' => $data['options']['showXScalePicker']['#value'],
    'showYScalePicker' => $data['options']['showYScalePicker']['#value'],
    'showAdvancedPanel' => $data['options']['showAdvancedPanel']['#value'],
    'state' => $data['options']['state']['#value'],
    'displayAnnotations' => $data['options']['displayAnnotations']['#value'],
    'displayAnnotationsFilter' => $data['options']['displayAnnotationsFilter']['#value'],
    'allowHtml' => $data['options']['allowHtml']['#value'],
    'displayDateBarSeparator' => $data['options']['displayDateBarSeparator']['#value'],
    'displayExactValues' => $data['options']['displayExactValues']['#value'],
    'displayLegendDots' => $data['options']['displayLegendDots']['#value'],
    'displayRangeSelector' => $data['options']['displayRangeSelector']['#value'],
    'displayZoomButtons' => $data['options']['displayZoomButtons']['#value'],
    'enableScrollWheel' => $data['options']['enableScrollWheel']['#value'],
    'showTipMap' => $data['options']['showTipMap']['#value'],
    'showLineMap' => $data['options']['showLineMap']['#value'],
    'lineColorMap' => $data['options']['lineColorMap']['#value'],
    'lineWidthMap' => $data['options']['lineWidthMap']['#value'],
    'zoomLevelMap' => $data['options']['zoomLevelMap']['#value'],
    'GaugeMin' => $data['options']['GaugeMin']['#value'],
    'GaugeMax' => $data['options']['GaugeMax']['#value'],
    'greenFrom' => $data['options']['greenFrom']['#value'],
    'greenTo' => $data['options']['greenTo']['#value'],
    'yellowFrom' => $data['options']['yellowFrom']['#value'],
    'yellowTo' => $data['options']['yellowTo']['#value'],
    'redFrom' => $data['options']['redFrom']['#value'],
    'redTo' => $data['options']['redTo']['#value'],
    'emlink' => $data['options']['emlink']['#value'],
    'emlinkforever' => $data['options']['emlinkforever']['#value'],
  );
  return $style_options;
}

function _vidi_tagmap_style_options($data) {
  $tablename = $data['tablename']['#value'];
  $field_names = array();
  if (db_table_exists($tablename)) {
    $results = db_query("show columns from {$tablename}");
    while ($result = db_fetch_object($results)) {
      $field_names[] = $result->Field;
    }
  }
  $table = array();
  foreach ($data['table'] as $key => $value) {
    if (in_array($key, $field_names)) {
      $table[$key] = array();
      if (is_array($value)) {
        $table[$key]['weight'] = $value['weight']['#value'];
        $table[$key]['add'] = $value['add']['#value'];
      }
    }
  }
  $style_options = array(
    'map_settings' => array(
      'width' => $data['map_settings']['width']['#value'],
      'height' => $data['map_settings']['height']['#value'],
      'clustering' => $data['map_settings']['clustering']['#value'],
      'max_visible_markers' => $data['map_settings']['max_visible_markers']['#value'],
      'min_markers_per_cluster' => $data['map_settings']['min_markers_per_cluster']['#value'],
      'max_lines_per_box' => $data['map_settings']['max_lines_per_box']['#value'],
      'initial_state' => array(
        'zoom' => $data['map_settings']['initial_state']['zoom']['#value'],
        'center' => $data['map_settings']['initial_state']['center']['#value'],
      ),
      'layers' => array(
        'layer' => $data['map_settings']['layers']['layer']['#value'],
        'overlay' => $data['map_settings']['layers']['overlay']['#value'],
        'defaultlayer' => $data['map_settings']['layers']['defaultlayer']['#value'],
      ),
      'embed_link' => $data['map_settings']['embed_link']['#value'],
      //za embed admin
      'view_link' => 'admin/build/views/edit/vidi_' . $data['tablename']['#value'],
      'storage' => $data['serialized']['#value'],
      'title' => $data['title']['#value'],
      'vidi_link' => $data['url']['#value'],
    ),
    'description_title' => $data['description_title']['#value'],
    'table' => $table,
    'color' => $data['color']['#value'],
    'steps' => $data['steps']['#value'],
    'fontsize' => $data['fontsize']['#value'],
    'fontstyle' => $data['fontstyle']['#value'],
    'latfield' => $data['latfield']['#value'],
    'lonfield' => $data['lonfield']['#value'],
    'weightfield' => $data['weightfield']['#value'],
    'tagfield' => $data['tagfield']['#value'],
    'imagecache_icon' => $data['imagecache_icon']['#value'],
    'imagefield' => $data['imagefield']['#value'],
  );
  return $style_options;
}

function _vidi_timelinemap_style_options($data) {
  $tablename = $data['tablename']['#value'];
  $field_names = array();
  if (db_table_exists($tablename)) {
    $results = db_query("show columns from {$tablename}");
    while ($result = db_fetch_object($results)) {
      $field_names[] = $result->Field;
    }
  }
  $table = array();

  foreach ($data['table'] as $key => $value) {
    if (in_array($key, $field_names)) {
      $table[$key] = array();
      if (is_array($value)) {
        $table[$key]['weight'] = $value['weight']['#value'];
        $table[$key]['added'] = $value['added']['#value'];
      }
    }
  }
  $style_options = array(
    'fields' => array(
      'title' => $data['fields']['title']['#value'],
      'link' => 0,
      'icon' => '',
      'advanced' => array(),
      'start' => $data['fields']['start']['#value'],
      'end' => $data['fields']['end']['#value'],
      'latitude' => $data['fields']['latitude']['#value'],
      'longitude' => $data['fields']['longitude']['#value'],
      'table' => $table
    ),
    'grouping' => 'title',
    'datasource' => $data['datasource']['#value'],
    'pathcolor' => $data['pathcolor']['#value'],
    'colors' => array(
      'values' => $data['colors']['#value'],
    ),
    'bands' => array(
      'band1_unit' => $data['band1_unit']['#value'],
      'band2_unit' => $data['band2_unit']['#value'],
    ),
    'display' => array(
      'width' => $data['display']['width']['#value'],
      'tlheight' => $data['display']['tlheight']['#value'],
      'mheight' => $data['display']['mheight']['#value'],
    ),
  );
  return $style_options;
}

function _vidi_view_field_definition($data) {
  $pk = NULL;
  $fields = array();

  //Check if table with the given tablename exists (already unprefixed)
  if (db_table_exists($data['tablename']['#value'])) {
    $results = db_query("show columns from {$data['tablename']['#value']}");
    while ($result = db_fetch_object($results)) {
      $columns[] = $result;
      if ($result->Key == 'PRI') {
        //Only one primary key alowed
        if (empty($pk)) {
          $pk = $result;
        }
        else {
          return FALSE;
        }
      }
    }
  }
  else {
    return FALSE;
  }
  foreach ($columns as $column) {
    $fields[$column->Field] = array(
      'id' => $column->Field,
      'table' => $data['tablename']['#value'],
      'field' => $column->Field,
      'label' => $column->Field,
      'exclude' => 0,
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
    );
  }

  return $fields;
}

function _vidi_return_view($data, $style, $style_options) {
  $view = views_get_view('vidi_' . $data['tablename']['#value']);
  if (!empty($view)) {
    $view->init_display();
    $view->init_handlers();
    $view->init_query();
    $view->init_style();
    return $view;
  }
  $view = _vidi_create_default_view($data, $style, $style_options);
  return $view;
}

function _vidi_create_default_view($data, $style, $style_options) {
  $fields = _vidi_view_field_definition($data);
  if (empty($fields)) {
    return FALSE;
  }
    
  $view = new view;
  $view->name = 'vidi_' . $data['tablename']['#value'];
  $view->description = 'Vidi visualization of ' . $data['tablename']['#value'] . 'table.';
  $view->tag = 'vidi';
  $view->view_php = '';
  $view->base_table = $data['tablename']['#value'];
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to TRUE to make a default view disabled initially */
  $handler = $view->new_display('default', 'Default', 'default');

  $handler->override_option('fields', $fields);
  $handler->override_option('title', t('Contents of !tablename', array('!tablename' => $data['tablename']['#value'])));
  $handler->override_option('header', t('This is your visualization of the !tablename table', array('!tablename' => $data['tablename']['#value'])));
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('empty', 'There are no rows in this table.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 0);
  $handler->override_option('use_pager', '0');
  $handler->override_option('style_plugin', $style);
  $handler->override_option('style_options', $style_options);

  return $view;
}

function _vidi_attach_view_display($view, $style, $style_options, $form_state, $data) {
  $cnt = 1;
  foreach ($view->display as $key => $value) {
    $key_array = explode('_', $key);
    if ($form_state['values']['view_type'] == $key_array[0]) {
      $cnt++;
    }
  }
  $new_display_id = $form_state['values']['view_type'] . '_' . $cnt;
  $handler = $view->new_display($form_state['values']['view_type'], $form_state['values']['display_name'], $new_display_id);
  $fields = _vidi_view_field_definition($data);
  if (!empty($fields)) {
    $handler->override_option('fields', $fields);
  }
  $handler->override_option('style_plugin', $style);
  if ($form_state['values']['view_type'] == 'page') {
    $handler->override_option('path', "vidi/$view->name/page_$cnt" );
    $handler->override_option('menu', array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
    ));
    $handler->override_option('tab_options', array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
    ));
  }
  $handler->override_option('style_options', $style_options);
  $view->save();
  if ($form_state['values']['view_type'] == 'page') {
    menu_rebuild();
  }
  $message = "View successfuly created.";
  if ($form_state['values']['view_type'] == 'page') {
    $message .= "<br/>" .  t("Your visualization is ") . l(t("here"), "vidi/$view->name/page_$cnt");
  }
  return array(0 => TRUE, 1 => $message);
}

function vidi_view_setup_operations_form($form_state) {
  if (empty($_GET['source']) || empty($_GET['build_id'])) {
    drupal_set_message(t('Please use the appropriate form to visualize your data.'));
    drupal_goto('vidi/chose_visualization_type');
  }
  $form['view_type'] = array(
    '#type' => 'select',
    '#title' => t('Type of display'),
    '#options' => array('page' => 'Page', 'block' => 'Block'),
    '#description' => t('This is where you decide if you want your visualization' .
    'across the whole page, or in a block.'),
    '#required' => TRUE
  );

  $form['display_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Display name'),
    '#required' => TRUE
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save'
  );

  return $form;
}

function vidi_view_setup_operations_form_submit($form, &$form_state) {
  $style = $_GET['source'];
  $build_id = $_GET['build_id'];
  $visualisation_form = $_SESSION["$build_id"];
  unset($_SESSION["$build_id"]);

  //Export the table definition, if it isn't exported already
  $sql = "SELECT COUNT(*) FROM {tw_tables} WHERE tablename = '%s'";
  $cnt = db_result(db_query($sql, schema_unprefix_table($visualisation_form['tablename']['#value'])));
  if ($cnt == 0) {
    tw_add_tables(array(schema_unprefix_table($visualisation_form['tablename']['#value'])), FALSE, FALSE);
  }

  $style_options = call_user_func('_vidi_' . $style . '_style_options', $visualisation_form);
  $view = _vidi_return_view($visualisation_form, $style, $style_options);

  $result = array();
  if ($view) {
    $result = _vidi_attach_view_display($view, $style, $style_options, $form_state, $visualisation_form);
  }
  if (!empty($result) && $result[0]) {
    drupal_set_message($result[1]);
  }
}