<?php

/**
 * @file
 * Form which controls the Timelinemap
 */
 
function theme_vidi_timelinemap_form_prefix($storage, $embed_str, $link) {
  $output = '<div id = "timelinemap-all-vidi-preview" class = "timelinemap-all">';
  $output .= '<div class = "timelinecontainer" style = "width: '. $storage['width'] . '; height:' . $storage['tlheight'] . '">';
  $output .= '<div id = "timeline-vidi-preview" class = "timeline"></div>';
  $output .= '</div>';
  $output .= '<div class = "mapcontainer" style = "width: '. $storage['width'] . '; height:' . $storage['mheight'] . '">';
  $output .= '<div id = "map-vidi-preview" class = "map"></div>';
  $output .= '</div>';
  $output .= '</div>' . $embed_str . '<div id = "back-button">' . $link . '</div><div class = "clear"></div>';
  return $output;
}

function vidi_timelinemap_form($form_state) {
  if (!module_exists('timelinemap')) {
    drupal_set_message(t('To use timelinemap visualization, please download the timelinemap module from <a href="@timelinemap-module-download">here</a>', array('@timelinemap-module-download' => 'http://drupal.org/project/timelinemap')));
    drupal_goto('vidi/chose_visualization_type');
  }
  global $db_type;
  $timelinemap_mod_path = drupal_get_path('module', 'timelinemap');
  drupal_add_css(drupal_get_path('module', 'vidi') . '/css/vidi-timelinemap.css');
  drupal_add_css($timelinemap_mod_path . '/css/timelinemap.css');
  drupal_add_js($timelinemap_mod_path . '/js/timemap.js');
  drupal_add_js($timelinemap_mod_path . '/js/timelinemap.js');

  $vidi_mod_path = drupal_get_path('module', 'vidi');

  $gmap_apikey = variable_get('googlemap_api_key', '');
  $timelinemap_apikey = $gmap_apikey ? $gmap_apikey : variable_get('timelinemap_api_key', '');
  $timelinemap_gapi = 'http://maps.google.com/maps?file=api&v=2&key='. $timelinemap_apikey;
  $timelinemap_sapi = 'http://static.simile.mit.edu/timeline/api/timeline-api.js';
  drupal_set_html_head('<script type="text/javascript" src="'. $timelinemap_gapi .'"></script>');
  drupal_set_html_head('<script type="text/javascript" src="'. $timelinemap_sapi .'"></script>');

  $options['numerical'] = array('' => '--');
  $options['string'] = array('' => '--');
  if (arg(2)) {
    $tablename = db_escape_table(arg(2));
    if (db_table_exists($tablename)) {
      if ($db_type == 'mysql' || $db_type == 'mysqli') {
        $results = db_query("SHOW COLUMNS FROM {$tablename}");
        while ($result = db_fetch_object($results)) {
          if ($result->Type == "float" || substr($result->Type, 0, 3) == "int") {
            $options['numerical'][$result->Field] = $result->Field;
          }
          else {
            $options['string'][$result->Field] = $result->Field;
          }
        }
      }
      elseif ($db_type == 'pgsql') {
        $results = db_query("SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.columns WHERE table_name = '" . $tablename . "'");
        while ($result = db_fetch_object($results)) {
          if ($result->data_type == "float" || substr($result->data_type, 0, 3) == "int") {
            $options['numerical'][$result->column_name] = $result->column_name;
          }
          else {
            $options['string'][$result->column_name] = $result->column_name;
          }
        }
      }
    }
  }
  $timelinemap_processed = !empty($form_state['storage']);
  $form['tablename'] = array('#type' => 'hidden', '#value' => $tablename);

  if (isset($_GET['fid'])) {
    if (!$timelinemap_processed) {
      $sql = "SELECT f.filepath AS filepath, te.form_state as storage FROM {files} f LEFT JOIN {timelinemap_embeds} te ON f.fid = te.fid WHERE f.fid = %d";
      $result = db_fetch_object(db_query($sql, $_GET['fid']));
      if (!empty($result)) {
        $path_arr = explode('/', $result->filepath);
        $filename = $path_arr[count($path_arr) -1];
        $filename_arr = explode('.', $filename);
        $form['filename'] = array(
          '#type' => 'hidden',
          '#value' => $filename_arr[0],
        );

        $form_state['storage'] = unserialize($result->storage);
        $form_state['storage']['filename'] = $filename_arr[0];
        $timelinemap_processed = TRUE;
      }
    }
    else {
      $form['filename'] = array(
        '#type' => 'hidden',
        '#value' => $form_state['storage']['filename'],
      );
    }
  }

  if ($timelinemap_processed) {

    $output = _vidi_prepare_timelinemap_data($form_state['storage']);

    //For embedding
    global $user;
    $embed_str = "";
    $tlheight = str_replace('em', '', $form_state['storage']['tlheight']);
    if ($tlheight == $form_state['storage']['tlheight']) {
      $meassurement_tl = 'px';
    }
    else {
      $meassurement_tl = 'em';
    }
    $tlheight = str_replace('px', '', $tlheight);
    $mheight = str_replace('em', '', $form_state['storage']['mheight']);
    if ($mheight == $form_state['storage']['mheight']) {
      $meassurement_m = 'px';
    }
    else {
      $meassurement_m = 'em';
    }
    $mheight = str_replace('px', '', $mheight);
    $height = $tlheight + $mheight;
    if (user_access('embed timeline map') && $form_state['storage']['embed_link'] && ($meassurement_m == $meassurement_tl)) {
      $embed_str .= '<div id="timelinemap-embed-wrapper">';
      if (isset($form['filename'])) {
        $embed_str .= '<a id="timelinemap-preview-embed" overwrite="' . $form['filename']['#value'] . '" class="timelinemap-embed-link" rel="timelinemap-all-vidi-preview" attrwidth="' . $form_state['storage']['width'] . '" attrheight="' . $height . $meassurement_m . '"  href="javascript: void(0);">[embed]</a></div>';
      }
      else {
        $embed_str .= '<a id="timelinemap-preview-embed" overwrite="" class="timelinemap-embed-link" rel="timelinemap-all-vidi-preview" attrwidth="' . $form_state['storage']['width'] . '" attrheight="' . $height . $meassurement_m . '"  href="javascript: void(0);">[embed]</a></div>';
      }
    }

    if ($_GET['new_data'] == 'TRUE') {
      $link = l(t('Try another type'), 'admin/content/vidi/chose_visualization_type', array('html' => TRUE, 'query' => 'tablename=' . $form_state['storage']['tablename']));
    }
    else {
      $link = l(t('Try another type'), 'admin/content/vidi/chose_visualization_type', array('html' => TRUE));
    }
    $prefix_str = theme('vidi_timelinemap_form_prefix', $form_state['storage'], $embed_str, $link);
    $form['#prefix'] = $prefix_str;
  }

  $datasource = $form_state['storage']['datasource'] ? $form_state['storage']['datasource'] : ($_GET['datasource'] ? $_GET['datasource'] : 'basic');
  $form['datasource'] = array(
    '#type' => 'hidden',
    '#default_value' => $datasource,
    '#multiple' => FALSE,
  );
  $form['visualization_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($form_state['storage']['visualization_title']) ? $form_state['storage']['visualization_title'] : '',
    '#size' => 60,
    '#required' => TRUE,
    '#description' => 'Title of the visualization.',
  );
  //display
  $form['display'] = array('#type' => 'fieldset', '#title' => t('Display settings'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['display']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($form_state['storage']['display']['width']) ? $form_state['storage']['display']['width'] : variable_get('timelinemap_default_width', '100%'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('The width of the timeline and map (in units of em, px or %), e.g. 600px or 90%. Leave blank to use default value.'),
    '#required' => TRUE
  );
  $form['display']['tlheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeline height'),
    '#default_value' => isset($form_state['storage']['display']['tlheight']) ? $form_state['storage']['display']['tlheight'] : variable_get('timelinemap_default_tlheight', '400px'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('The height of the timeline (in units of em or px), e.g. 400px. Leave blank to use default value.'),
    '#required' => TRUE,
  );
  $form['display']['mheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Map height'),
    '#default_value' => isset($form_state['storage']['display']['mheight']) ? $form_state['storage']['display']['mheight'] : variable_get('timelinemap_default_mheight', '400px'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('The height of the map (in units of em or px), e.g. 400px. Leave blank to use default value.'),
    '#required' => TRUE
  );
  $form['display']['embed_link'] = array(
    '#type' => 'checkbox',
    '#weight' => 4,
    '#title' => t('Display embed link'),
    '#return_value' => 1,
    '#default_value' => isset($form_state['storage']['embed_link']) ? $form_state['storage']['embed_link'] : '1',
  );
  //fields mapping
  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field usage'),
    '#collapsible' => TRUE, '#collapsed' => FALSE,
    '#description' => t('Select the fields that contain the title, start time and end time of each item'),
  );
  $form['fields']['title'] = array(
    '#type' => 'select',
    '#title' => t('Title'),
    '#options' => $options['string'],
    '#default_value' => $form_state['storage']['title'],
    '#required' => TRUE,
  );
  $desc_fields = $options['string'];
  $form['table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields to be displayed in the info cloud'),
    '#collapsible' => TRUE,
    '#theme' => 'timelinemap_rearrange_form',
    '#tree' => TRUE
  );
  $count = 0;
  $string_options = $options['string'];
  $fs = array_shift($string_options);
  $num_options = $options['numerical'];
  $fn = array_shift($num_options);
  $sort_array = array_merge($string_options, $num_options);
  if (isset($form_state['storage']['table']) && count($form_state['storage']['table'] == count(array_merge($options['string'], $options['numerical'])))) {
    $sort_array = $form_state['storage']['table'];
    uasort($sort_array, '_timelinemap_element_sort');
    $sort_array = array_keys($sort_array);
    $tmp = array();
    foreach ($sort_array as $value)
      $tmp[$value] = $value;
    $sort_array = $tmp;
    unset($tmp);
  }
  foreach ($sort_array as $id => $field) {
    $form['table'][$id] = array('#tree' => TRUE);
    $form['table'][$id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 200,
      '#default_value' => ++$count,
    );
    $form['table'][$id]['name'] = array(
      '#value' => $field,
    );

    $form['table'][$id]['added'] = array(
      '#type' => 'checkbox',
      '#id' => 'views-added-' . $id,
      '#attributes' => array('class' => 'views-add-checkbox'),
      '#default_value' => isset($form_state['storage']['table'][$id]['added']) ? $form_state['storage']['table'][$id]['added']:0,
    );
  }

  // Add javascript settings that will be added via $.extend for tabledragging
  $form['#js']['tableDrag']['arrange']['weight'][0] = array(
    'target' => 'weight',
    'source' => NULL,
    'relationship' => 'sibling',
    'action' => 'order',
    'hidden' => TRUE,
    'limit' => 0,
  );

  //start and end date for timeline
  $form['fields']['start'] = array(
    '#type' => 'select',
    '#title' => t('Start'),
    '#options' => $options['string'],
    '#default_value' => $form_state['storage']['start'],
    '#required' => TRUE,
  );

  $form['fields']['end'] = array(
    '#type' => 'select',
    '#title' => t('End'),
    '#options' => $options['string'],
    '#default_value' => isset($form_state['storage']['end']) ? $form_state['storage']['end'] : '',
    '#process' => array('views_process_dependency'),
    '#dependency' => array('edit-style-options-datasource' => array('basic')),
  );

  $form['fields']['latitude'] = array(
    '#type' => 'select',
    '#title' => 'Latitude field',
    '#options' => $options['numerical'],
    '#default_value' => $form_state['storage']['latitude'],
    '#required' => TRUE,
  );

  $form['fields']['longitude'] = array(
    '#type' => 'select',
    '#title' => t('Longitude field'),
    '#options' => $options['numerical'],
    '#default_value' => $form_state['storage']['longitude'],
    '#required' => TRUE,
  );

  $form['color_schemes'] = array('#type' => 'fieldset', '#title' => t('Color schemes'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  //pathcolor for pathline type
  $form['color_schemes']['pathcolor'] = array(
    '#title' => t('Path color'),
    '#type' => 'textfield',
    '#process' => array('views_process_dependency'),
    '#dependency' => array('edit-style-options-datasource' => array('pathline')),
    '#default_value' => isset($form_state['storage']['pathcolor']) ? $form_state['storage']['pathcolor'] : '',
    '#description' => t('Enter one hexadecimal RGB color value per line. Examples: #FF00FF #FF0099 #998811')
  );
  $form['color_schemes']['colors']= array(
    '#type' => 'select',
    '#title' => t('Color coding'),
    '#options' => array('city_funky' => t('City Funky'), 'pure_classic' => t('Pure Classic'), 'ecology_green' => t('Ecology Green'), 'clear_blue' => t('Clear Blue'), 'lavender' => t('Lavender'), 'burgundy_red' => t('Burgundy Red'), 'orange_sun' => t('Orange Sun'), 'vidi_special' => t('VIDI Special')),
    '#default_value' => isset($form_state['storage']['colors']) ? $form_state['storage']['colors'] : '',
  );

  // Band/interval settings
  $form['bands'] = array('#type' => 'fieldset', '#title' => t('Band/interval settings'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $intervals = array('SECOND' => t('second'), 'MINUTE' => t('minute'), 'HOUR' => t('hour'), 'DAY' => t('day'), 'WEEK' => t('week'), 'MONTH' => t('month'), 'YEAR' => t('year'), 'DECADE' => t('decade'), 'CENTURY' => t('century'), 'MILLENNIUM' => t('millennium'));
  $form['bands']['band1_unit'] = array(
    '#type' => 'select',
    '#title' => t('Main band interval unit'),
    '#default_value' => isset($form_state['storage']['band1_unit']) ? $form_state['storage']['band1_unit'] : 'year',
    '#options' => $intervals,
    '#description' => t(''),
  );
  $form['bands']['band2_unit'] = array(
    '#type' => 'select',
    '#title' => t('Summary band interval unit'),
    '#default_value' => isset($form_state['storage']['band2_unit']) ? $form_state['storage']['band2_unit'] : 'decade',
    '#options' => array_merge(array('' => t('Not displayed')), $intervals),
    '#description' => t(''),
  );

  $form['submit_preview'] = array(
    '#value' => t('Preview'),
    '#type' => 'submit',
  );

  if (user_access('administer views')) {
    $form['submit_save'] = array(
      '#value' => t('Make a view'),
      '#type' => 'submit',
    );
  }

  return $form;
}

function vidi_timelinemap_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit-preview') {
    $form_state['storage'] = $form_state['values'];
  }
  if ($form_state['clicked_button']['#id'] == 'edit-submit-save') {
    if (user_access('administer views')) {
      $build_id = $form_state['values']['form_build_id'];
      $_SESSION["$build_id"] = $form;
      unset($form_state['storage']);
      $form_state['redirect'] = array('vidi/view_generation', "build_id=$build_id&source=timelinemap");
    }
  }
}

function _vidi_prepare_timelinemap_data($values) {
  $sql = "SELECT * FROM {" . $values['tablename'];
  $results = db_query($sql);
  $i=0;

  $sort_array = $values['table'];
  uasort($sort_array, '_timelinemap_element_sort');
  foreach ($sort_array as $key => $value) {
    if ($value['added'] == 0) {
      unset($sort_array[$key]);
    }
  }
  $sort_array = array_keys($sort_array);

  if (count($sort_array) != 0) {
    $tmp = array();
    foreach ($sort_array as $value)
      $tmp[$value] = $value;
    $sort_array = $tmp;
    unset($tmp);
  }

  while ($result = db_fetch_array($results)) {
    $data['events'][$i]['descriptiontext_new'][] = array();
    if (is_array($sort_array)) {
      foreach ($sort_array as $val) {
        $data['events'][$i]['descriptiontext_new'][] = $result[$val];
      }
    }
    $data['events'][$i]['title'] = $result[$values['title']];
    $data['events'][$i]['description'] = $result[$values['description']];
    $data['events'][$i]['start'] = $result[$values['start']];
    $data['events'][$i]['end'] = $result[$values['end']];
    $data['events'][$i]['latitude'] = (float)$result[$values['latitude']];
    $data['events'][$i]['longitude'] = (float)$result[$values['longitude']];
    $data['events'][$i]['color'] = _vidi_get_color($values['colors'], $i);
    $data['events'][$i]['video'] = $result[$values['video']];
    $i++;
  }
  $data['datasource'] = $values['datasource'];
  $data['pathcolor'] = $values['pathcolor'];
  $data['band1_unit'] = $values['band1_unit'];
  $data['band2_unit'] = $values['band2_unit'];
  $data['width'] = $values['width'];
  $data['tlheight'] = $values['tlheight'];
  $data['mheight'] = $values['mheight'];
  $data['filename'] = isset($_GET['filename'])?$_GET['filename']:"";
  $data['display_name'] = 'vidi-preview';

  $data['storage'] = serialize($values);
  $data['vidi_link'] = $_GET['q'];
  $data['datasource'] = $_GET['datasource'];
  $data['view_link'] = '';

  drupal_add_js(array('timelinemap' =>  array($data['display_name'] => $data)), "setting");
  drupal_add_js(array('timelinemap_path' => drupal_get_path('module', 'timelinemap')), 'setting');

  return $data;
}

function _vidi_get_color($values, $id) {
  switch ($values) {
    case 'city_funky':
      $colors_values = array('yellow', 'green', 'light blue', 'orange', 'purple');
      break;
    case 'pure_classic':
      $colors_values = array('blue');
      break;
    case 'ecology_green':
      $colors_values = array('green');
      break;
    case 'clear_blue':
      $colors_values = array('light blue');
      break;
    case 'lavender':
      $colors_values = array('purple');
      break;
    case 'burgundy_red':
      $colors_values = array('red');
      break;
    case 'orange_sun':
      $colors_values = array('orange');
      break;
    case 'vidi_special':
      $colors_values = array('yellow', 'green', 'light blue', 'orange');
  }

  if (count($colors_values) != 0) {
    $colors = array_values($colors_values);
    $tmp = $id;
    while ($tmp >= count($colors)) {
      $tmp = $tmp - count($colors);
    }
    return $colors[$tmp];
  }
  return 'red';
}