<?php

/**
 * @file
 * vidi_admin.inc
 */

function theme_vidi_tables($form) {
  $select_header = theme('table_select_header_cell');
  $header = array(
    $select_header,
    t('Table name'),
    t('Added by'),
    t('Added on'),
  );

  $output = '';
  //$output .= drupal_render($form['options']);
  $rows = array();
  $counter = 0;
  foreach (element_children($form['tablename']) as $key) {
    $row = array();
    $row[] = drupal_render($form['delete'][$key]);
    $row[] = drupal_render($form['tablename'][$key]);
    $row[] = drupal_render($form['user'][$key]);
    $row[] = drupal_render($form['date'][$key]);
    $rows[] = $row;
    $counter++;
  }
  if ($counter == 0) {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '4'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function vidi_tables_admin_form($form_state) {
  global $db_type;
  $limit = 20;
  $results = pager_query("SELECT vvt.*, u.name FROM {vidi_visualization_tables} vvt LEFT JOIN {users} u on vvt.uid = u.uid ORDER BY vvt.tablename", $limit, 0);
  $existing = array();
  $excludes = array();
  while ($result = db_fetch_array($results)) {
    $existing[$result['tid']] = $result;
    $excludes[$result['tablename']] = $result['tablename'];
  }
  $results = db_query('SELECT tablename FROM {vidi_visualization_tables}');
  while ($result = db_fetch_object($results)) {
    $excludes[$result->tablename] = $result->tablename;
  }
  if ($db_type == 'mysql' || $db_type == 'mysqli') {
    $sql = "SHOW TABLES";
  }
  elseif ($db_type == 'pgsql') {
    $sql = "SELECT tablename FROM {pg_tables} ORDER BY tablename";
  }
  else {
    drupal_set_message(t('Unrecognized database type %dbtype', array('%dbtype' => $db_type)));
    return;
  }
  $results = db_query($sql);
  $options = array();
  while ($result = db_fetch_array($results)) {
    foreach ($result as $tablename) {
      $unprefixed = schema_unprefix_table($tablename);
      if (isset($excludes[$unprefixed])) {
        continue;
      }
        
      $options[$unprefixed] = $unprefixed;
    }
  }
  $options_existing = array();
  foreach ($existing as $key => $value) {
    $options_existing[$value['tid']] = '';
    $form['tables']['tablename'][$value['tid']] = array(
      '#value' => $value['tablename']
    );
    $form['tables']['user'][$value['tid']] = array(
      '#value' => isset($value['name'])?$value['name']:'anonymous'
    );

    list($date, $time) = explode(' ', $value['insert_time']);
    list($year, $month, $day) = explode('-', $date);
    list($hour, $minute, $second) = explode(':', $time);

    $form['tables']['date'][$value['tid']] = array(
      '#value' => date('M d, Y H:i:s', mktime((int)$hour, (int)$minute, (int)$second, (int)$month, (int)$day, (int)$year))
    );
  }
  $form['tables']['delete'] = array('#type' => 'checkboxes', '#options' => $options_existing);
  $form['tables']['pager'] = array('#value' => theme('pager', NULL, $limit, 0));
  $form['tables']['#theme'] = 'vidi_tables';
  $form['remove'] = array(
    '#type' => 'submit',
    '#value' => t('Remove selected'),
    '#submit' => array('vidi_remove_tables'),
  );
  $form['newtable_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add database tables'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (count($options) != 0) {
    $form['newtable_fieldset']['newtable'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => min(12, count($options)),
      '#options' => $options,
    );
  }
  else {
    $form['newtable_fieldset']['newtable'] = array(
      '#type' => 'item',
      '#value' => t('All the tables from the database are already in the VIDI visualization table!')
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

function vidi_tables_admin_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['newtable'])) {
    global $user;
    foreach ($form_state['values']['newtable'] as $value) {
      $table = new stdClass();
      $table->tablename = $value;
      $table->uid = $user->uid;
      $table->insert_time = date('Y-m-d H:i:s', time());
      drupal_write_record('vidi_visualization_tables', $table);
    }
  }
}

function vidi_remove_tables($form, &$form_state) {
  foreach ($form_state['values']['delete'] as $value) {
    if (!empty($value)) {
      db_query('DELETE FROM {vidi_visualization_tables} WHERE tid = %d', $value);
    }
      
  }
}