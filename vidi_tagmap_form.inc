<?php

/**
 * @file
 * Form which controls the tagmap display.
 */

function theme_vidi_tagmap_form_prefix($form_storage, $output_graph, $link) {
  $output = '<div width="'. $form_storage['width'] . '" height="' . $form_storage['height'] . '  ">';
  $output .= $output_graph;
  $output .= '</div> <div id = "back-button">' . $link . '</div>';
  return $output;
}

function vidi_tagmap_preview() {
  if (!module_exists('tagmap')) {
    drupal_set_message(t('To use tagmap visualization, please download the tagmap module from <a href="@tagmap-module-download">here</a>', array('@tagmap-module-download' => 'http://drupal.org/project/tagmap')));
    drupal_goto('vidi/chose_visualization_type');
  }
  module_load_include('inc', 'tagmap', 'tagmap.theme');
  tagmap_initialize();
  drupal_add_js(drupal_get_path('module', 'vidi') .'/js/vidi.js', 'module', 'header', FALSE, TRUE, FALSE);

  return drupal_get_form('vidi_tagmap_form');
}

function _vidi_prepare_tagmap_markers($values) {
  $output = '';
  $markers = array();

  //Sets the dimensions of the image to be rendered. Only if ImageCache module is present. Pixels only.
  $image_field = '';
  $imagecache_namespace = '';
  $width = NULL;
  $height = NULL;
  if (module_exists('imagecache') && $values['imagecache_icon'] != '--' && $values['imagefield'] && $values['imagefield'] != '--') {
    $image_field = $values['imagefield'];
    $imagecache_namespace = $values['imagecache_icon'];
    $preset_actions = imagecache_preset_actions(imagecache_preset_by_name($imagecache_namespace));
    foreach ($preset_actions as $preset) {
      if (isset($preset['data']['width']) && count(explode($preset['data']['width'], '%')) == 1) {
        if (!isset($width) || ((int)$width) < ((int)$preset['data']['width'])) {
          $width = $preset['data']['width'];
        }
      }
      if (isset($preset['data']['height']) && count(explode($preset['data']['height'], '%')) == 1) {
        if (!isset($height) || ((int)$height) < ((int)$preset['data']['height'])) {
          $height = $preset['data']['height'];
        }
      }
    }
    if (isset($width)) {
      $width .= 'px';
    }
    if (isset($height)) {
      $height .= 'px';
    }
  }

  $term_name_field = $values['tagfield'];

  $results = db_query("SELECT * FROM {" . $values['tablename'] . "}");
  while ($result = db_fetch_array($results)) {
    if (module_exists('imagecache') && $values['imagecache_icon'] != '--' && $values['imagefield'] && $values['imagefield'] !='--') {
      $image_full_path = $result[$values['imagefield']];
      $image_thumb_path = base_path() . imagecache_create_path($imagecache_namespace, $result[$image_field]);
    }
    $info_html = "";
    $sort_array = $values['table'];
    uasort($sort_array, '_tagmap_element_sort');
    foreach ($sort_array as $key => $value) {
      if ($value['add'] == 0) {
        unset($sort_array[$key]);
      }
    }
    $sort_array = array_keys($sort_array);

    if (count($sort_array) != 0 || isset($values['description_title']) || isset($image_thumb_path)) {
      $tmp = array();
      foreach ($sort_array as $value) {
        $tmp[$value] = $value;
      }
      $sort_array = $tmp;
      unset($tmp);

      $info_html .= '<div class="tagmap-view-info">';
      if (!empty($values['description_title'])) {
        $info_html .= '<div class="tagmap-view-info-title">' . $result[$values['description_title']] . '</div>';
      }

      if (isset($image_thumb_path)) {
        $style = "";
        if (isset($width)) {
          $style .= "width:" . $width . "; ";
        }
        if (isset($height)) {
          $style .= "height:". $height . "; ";
        }
        if (module_exists('lightbox2')) {
          $small_image = '<img src="' . $image_thumb_path . '"></img>';
          $info_html .= '<div style="' . $style . '" class="tagmap-cloud-image">' . l($small_image, $image_full_path, array('attributes' => array('onclick' => 'Lightbox.start(this, FALSE, FALSE, FALSE, FALSE); return FALSE;', 'class' => 'lightbox-processed'), 'html' => TRUE)) . '</div>';
        }
        else {
          $info_html .= '<div style="' . $style . '" class="tagmap-cloud-image"><img src="' . $image_thumb_path . '"></img></div>';
        }
      }
      $info_html .= '<div class="tagmap-view-info-content">';

      if (count($sort_array) != 0) {
        $info_html .= '<div class="tagmap-view-info-content">';
        $class = 0;
        foreach ($sort_array as $sort_array_value) {
          $info_html .= "<div class=\"tagmap-cloud-paragraph paragraph-" . $class++ . "\">" . $result[$sort_array_value] . '</div>';
        }
        $info_html .= '</div>';
      }
      $info_html .= '</div>';
    }

    $lat = (float)$result[$values['latfield']];
    $lon = (float)$result[$values['lonfield']];
    $weight = $result[$values['weightfield']];
    $markers[] = array(
      'latitude' => $lat,
      'longitude' => $lon,
      'weight' => $weight,
      'term' => $result[$term_name_field],
      'infoText' => $info_html,
      'minFontSize' => $values['fontsize'],
      'fontStyle' => $values['fontstyle'],
      'markerColor' => 'css',
    );
  }

  tagmap_generate_weighted_markers($markers, $values['steps'], $values['color']);
  if (!empty($markers)) {
    $map['markers'] = $markers;
    $map['embed_link'] = $values['embed_link'];
    if (isset($values['width'])) {
      $map['width'] = $values['width'];
    }
    if (isset($values['height'])) {
      $map['height'] = $values['height'];
    }
    if (isset($values['clustering'])) {
      $map['clustering'] = $values['clustering'];
      if ($values['clustering'] == 'clusterer2') {
        $map['maxVisibleMarkers'] = $values['max_visible_markers'];
        $map['minMarkersPerCluster'] = $values['min_markers_per_cluster'];
        $map['maxLinesPerBox'] = $values['max_lines_per_box'];
      }
    }
    if (isset($values['zoom'])) {
      $map['initial_state']['zoom'] = $values['zoom'];
    }
    if (isset($values['center'])) {
      $map['initial_state']['center'] = $values['center'];
    }
    if (isset($values['layer'])) {
      $map['layers']['layer'] = $values['layer'];
    }
    if (isset($values['overlay'])) {
      $map['layers']['overlay'] = $values['overlay'];
    }
    if (isset($values['defaultlayer'])) {
      $map['layers']['defaultlayer'] = $values['defaultlayer'];
    }
    unset($values['op']);
    unset($values['form_build_id']);
    unset($values['form_token']);
    unset($values['form_id']);

    //for embed admin
    $map['storage'] = serialize($values);
    $map['view_link'] = '';
    $map['vidi_link'] = empty($values['url']) ? implode('/', arg()) : $values['url'];
    $map['tagmap_title'] = $values['title'];
    $map['filename'] = isset($values['filename'])?$values['filename']:"";

    $output_final = theme('tagmap', array('#settings' => $map));
  }
  else {
    drupal_set_message(t('The tagmap has no markers to display. Choose some content to see the tagmap.'), 'status');
  }

  return $output_final;
}

function vidi_tagmap_form($form_state) {
  module_load_include('inc', 'tagmap', 'tagmap_color_presets');
  $tagmap_color_presets = _tagmap_color_presets();
  $preset_names = array('' => 'From CSS file');
  $processed = !empty($form_state['storage']);
  $defaults = array_merge(tagmap_defaults(), variable_get('tagmap_settings', array()));

  if (isset($_GET['fid'])) {
    if (!$processed) {
      $sql = "SELECT f.filepath AS filepath, te.form_state_storage as storage FROM {files} f LEFT JOIN {tagmap_embeds} te ON f.fid = te.fid WHERE f.fid = %d";
      $result = db_fetch_object(db_query($sql, $_GET['fid']));
      if (!empty($result)) {
        $path_arr = explode('/', $result->filepath);
        $filename = $path_arr[count($path_arr) -1];
        $filename_arr = explode('.', $filename);
        $form['filename'] = array(
          '#type' => 'hidden',
          '#value' => $filename_arr[0],
        );

        $form_state['storage'] = unserialize($result->storage);
        $form_state['storage']['filename'] = $filename_arr[0]; // overwrite bug patch
        $processed = TRUE;
      }
    }
    else {
      $form['filename'] = array(
        '#type' => 'hidden',
        '#value' => $form_state['storage']['filename'],
      );
    }
  }

  if ($processed) {
    $output = _vidi_prepare_tagmap_markers($form_state['storage']);
    $link = l(t('Try another visualization type'), 'vidi/chose_visualization_type', array('query' => 'tablename=' . $form_state['storage']['tablename']));
    $prefix_str = theme('vidi_tagmap_form_prefix', $form_state['storage'], $output, $link);
    $form['#prefix'] = $prefix_str;
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name your tagmap'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['storage']['title'])?$form_state['storage']['title']:'',
    '#maxlength' => 60,
    '#size' => 60,
  );

  $form['map_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

  );

  $form['map_settings']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Map width, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>.'),
    '#default_value' => isset($form_state['storage']['width'])?$form_state['storage']['width']:$defaults['width'],
    '#maxlength' => 10,
    '#size' => 10,
    '#required' => TRUE,

  );

  $form['map_settings']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Map height, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>.'),
    '#default_value' => isset($form_state['storage']['height'])?$form_state['storage']['height']:$defaults['height'],
    '#maxlength' => 10,
    '#size' => 10,
    '#required' => TRUE,

  );

  $form['map_settings']['initial_state'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set the initial state of the map'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('These settings are optional. If you do not enter the values, they will be calculated on the basis of the markers position on the map.'),

  );

  $form['map_settings']['initial_state']['zoom'] = array(
    '#type' => 'select',
    '#title' => t('Default zoom'),
    '#default_value' => isset($form_state['storage']['zoom'])?$form_state['storage']['zoom']:$defaults['initial_state']['zoom'],
    '#options' => array(
      -1 => t('None')
      ) + drupal_map_assoc(range(0, 17)),
    '#description' => t('The default zoom level of the map.'),

  );

  $form['map_settings']['initial_state']['center'] = array(
    '#type' => 'textfield',
    '#title' => t('Default center'),
    '#default_value' => isset($form_state['storage']['center'])?$form_state['storage']['center']:$defaults['initial_state']['center'],
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('The default center coordinates of the map, expressed as a decimal latitude and longitude, separated by a comma.'),
    '#element_validate' => array(
      'tagmap_center_validate'
    ),

  );

  $form['map_settings']['layers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add additional layers and overlays to the map'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Here you can add additional layers and/or overlays to the map. You have to define them ahead of time on the !administration page',
    array('!administration' => user_access('administer site configuration') ? l(t('administration'), 'admin/settings/tagmap/layers/addlayer', array('attributes' => array('target' => '_blank'))) : 'administration')),
  );
  $results = db_query("SELECT * FROM {tagmap_layers}");
  $layers = array();
  $overlays = array('' => t('None'));
  $default_layers = array();
  while ($result = db_fetch_object($results)) {
    if ($result->baselayer) {
      $layers[$result->lid] = $result->title;
      $default_layers[$result->name] = $result->title;
    }
    else {
      $overlays[$result->lid] = $result->title;
    }
      

  }
  $form['map_settings']['layers']['layer'] = array(
    '#type' => 'select',
    '#title' => t('Select layers to add'),
    '#multiple' => TRUE,
    '#options' => $layers,
    '#default_value' => isset($form_state['storage']['layer']) ? $form_state['storage']['layer'] : ''
  );
  $form['map_settings']['layers']['overlay'] = array(
    '#type' => 'select',
    '#title' => t('Select an overlay to add'),
    '#options' => $overlays,
    '#default_value' => isset($form_state['storage']['overlay']) ? $form_state['storage']['overlay'] : ''
  );

  foreach ($defaults['baselayers'] as $key => $value) {
    if ($value) {
      $default_layers[$key] = $key;
    }
  }

  $form['map_settings']['layers']['defaultlayer'] = array(
    '#type' => 'select',
    '#title' => t('Select the default map layer'),
    '#options' => $default_layers,
    '#default_value' => isset($form_state['storage']['defaultlayer']) ? $form_state['storage']['defaultlayer'] : $defaults['maptype']
  );

  if (file_exists(drupal_get_path('module', 'tagmap') .'/js/markerclusterer_packed.js') || file_exists(drupal_get_path('module', 'tagmap') .'/js/Clusterer2.js')) {
    $opts = array('' => t('None'));
    if (file_exists(drupal_get_path('module', 'tagmap') .'/js/markerclusterer_packed.js')) {
      $opts['markerclusterer_packed'] = 'markerclusterer_packed.js';
    }
    if (file_exists(drupal_get_path('module', 'tagmap') .'/js/Clusterer2.js')) {
      $opts['clusterer2'] = 'Clusterer2.js';
    }
    $form['map_settings']['clustering'] = array(
      '#type' => 'select',
      '#title' => t('Use clusters for marker management'),
      '#options' => $opts,
      '#default_value' => (isset($form_state['storage']['clustering']) ? $form_state['storage']['clustering'] : ''),
    );

    $form['map_settings']['max_visible_markers'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of visible markers'),
      '#prefix' => '<div class="vidi-tagmap-clusterer-fields">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['storage']['max_visible_markers'])?$form_state['storage']['max_visible_markers']:'150',
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t('The maximum number of visible markers when using Clusterer2.js library.'),
    );
    $form['map_settings']['min_markers_per_cluster'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimum number of markers per cluster'),
      '#prefix' => '<div class="vidi-tagmap-clusterer-fields">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['storage']['min_markers_per_cluster'])?$form_state['storage']['min_markers_per_cluster']:'5',
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t('The minimum number of markers per cluster when using Clusterer2.js library.'),
    );
    $form['map_settings']['max_lines_per_box'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of lines'),
      '#prefix' => '<div class="vidi-tagmap-clusterer-fields">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['storage']['max_lines_per_box'])?$form_state['storage']['max_lines_per_box']:'10',
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t('The maximum number of lines in the info box when using Clusterer2.js library.'),
    );
  }
  $form['map_settings']['embed_link'] = array(
    '#type' => 'checkbox',
    '#weight' => 4,
    '#title' => t('Display embed link'),
    '#return_value' => 1,
    '#default_value' => isset($form_state['storage']['embed_link']) ? $form_state['storage']['embed_link'] : '1',
  );

  $form['steps'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag levels'),
    '#description' => t('Number of different tag levels that will be generated. Must be an integer value.'),
    '#default_value' => $form_state['storage']['steps']?$form_state['storage']['steps']:6,
    '#maxlength' => 10,
    '#size' => 10,
    '#required' => TRUE,

  );

  $form['fontsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Font size in pixels'),
    '#description' => t('Minimum font size for the first level terms.'),
    '#default_value' => $form_state['storage']['fontsize']?$form_state['storage']['fontsize']:10,
    '#maxlength' => 10,
    '#size' => 10,
    '#required' => TRUE,

  );

  $font_style = array(
    'normal' => t('Normal'),
    'italic' => t('Italic'),
    'oblique' => t('Oblique')
  );

  $form['fontstyle'] = array(
    '#type' => 'select',
    '#title' => t('Font style'),
    '#default_value' => isset($form_state['storage']['fontstyle'])?$form_state['storage']['fontstyle']:$font_style['normal'],
    '#options' => $font_style,
    '#required' => TRUE,
  );

  $form['color'] = array(
    '#type' => 'select',
    '#title' => t('Color preset'),
    '#default_value' => isset($form_state['storage']['color']) ? $form_state['storage']['color'] : $preset_names[''],
    '#options' => $preset_names + $tagmap_color_presets,
  );

  $options = array(
    'numerical' => array(),
    'string' => array()
  );


  $tablename_prefixed = db_escape_table(arg(2));
  $tablename = schema_unprefix_table($tablename_prefixed);
  if (db_table_exists($tablename)) {
    $results = db_query("show columns from {$tablename}");
    while ($result = db_fetch_object($results)) {
      if ($result->Type == "float" || substr($result->Type, 0, 3) == "int") {
        $options['numerical'][$result->Field] = $result->Field;
      }
      else {
        $options['string'][$result->Field] = $result->Field;
      }
    }
  }

  $form['tablename'] = array('#type' => 'hidden', '#value' => $tablename);

  $form['url'] = array('#type' => 'hidden', '#value' => $_GET['q']); //for embed admin

  $form['latfield'] = array(
    '#title' => t('Latitude field'),
    '#description' => t('Format must be degrees decimal.'),
    '#type' => 'select',
    '#options' => $options['numerical'],
    '#default_value' => $form_state['storage']['latfield'],
    '#required' => TRUE,
  );

  $form['lonfield'] = array(
    '#title' => t('Longitude field'),
    '#description' => t('Format must be degrees decimal.'),
    '#type' => 'select',
    '#options' => $options['numerical'],
    '#default_value' => $form_state['storage']['lonfield'],
    '#required' => TRUE,
  );

  $form['description_title'] = array(
    '#title' => t('Title'),
    '#description' => t('The title of the description cloud.'),
    '#type' => 'select',
    '#options' => array_merge(array('' => t('None')), $options['string']),
    '#default_value' => $form_state['storage']['description_title'],
  );

  $form['table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields to be displayed in the info cloud'),
    '#theme' => 'tagmap_rearrange_form',
    '#tree' => TRUE
  );

  $count = 0;
  $sort_array = array_merge($options['string'], $options['numerical']);
  if (isset($form_state['storage']['table']) && count($form_state['storage']['table'] == count(array_merge($options['string'], $options['numerical'])))) {
    $sort_array = $form_state['storage']['table'];
    uasort($sort_array, '_tagmap_element_sort');
    $sort_array = array_keys($sort_array);
    $tmp = array();
    foreach ($sort_array as $value)
      $tmp[$value] = $value;
    $sort_array = $tmp;
    unset($tmp);
  }

  foreach ($sort_array as $id => $field) {
    $form['table'][$id] = array('#tree' => TRUE);
    $form['table'][$id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 200,
      '#default_value' => ++$count,
    );

    $form['table'][$id]['name'] = array(
      '#value' => $field,
    );

    $form['table'][$id]['add'] = array(
      '#type' => 'checkbox',
      '#id' => 'views-removed-' . $id,
      '#attributes' => array('class' => 'tagmap-remove-checkbox'),
      '#default_value' => isset($form_state['storage']['table'][$id]['add'])?$form_state['storage']['table'][$id]['add']:0,
    );
  }
  
  // Add javascript settings that will be added via $.extend for tabledragging
  $form['#js']['tableDrag']['arrange']['weight'][0] = array(
    'target' => 'weight',
    'source' => NULL,
    'relationship' => 'sibling',
    'action' => 'order',
    'hidden' => TRUE,
    'limit' => 0,
  );

  $form['weightfield'] = array(
    '#title' => t('Tag weight field'),
    '#required' => TRUE,
    '#description' => t('This field determines the size of the corresponding tag.'),
    '#type' => 'select',
    '#options' => $options['numerical'],
    '#default_value' => $form_state['storage']['weightfield'],

  );

  $form['tagfield'] = array(
    '#title' => t('Tag field'),
    '#description' => t('String that will be displayed over map.'),
    '#type' => 'select',
    '#options' => array_merge(array(
    '' => ''
    ), $options['string']),
    '#default_value' => $form_state['storage']['tagfield'],
    '#required' => TRUE,
  );

  if (module_exists('imagecache')) {
    $imagecache_presets = array('--' => t('--'));

    foreach (imagecache_presets() as $preset) {
      $imagecache_presets[$preset['presetname']] = $preset['presetname'];
    }

    $form['imagecache_icon'] = array(
      '#type' => 'select',
      '#title' => t('Imagecache preset for Image'),
      '#options' => $imagecache_presets,
      '#description' => t('Imagecache preeset. image dimensions should be in px. Not necessary, but recomended.'),
      '#default_value' => $form_state['storage']['imagecache_icon'],

    );

    $form['imagefield'] = array(
      '#title' => t('Image field'),
      '#type' => 'select',
      '#options' => array_merge(array('--' => '--'), $options['string']),
      '#default_value' => $form_state['storage']['imagefield'],
      '#description' => t("This field should be excluded from display so the path to the image doesn't render."),
    );
  }

  $form['submit_preview'] = array(
    '#value' => t('Preview'),
    '#type' => 'submit',
  );

  if (user_access('administer views')) {
    $form['submit_save'] = array(
      '#value' => t('Save>>'),
      '#type' => 'submit',
    );
  }

  return $form;
}

function vidi_tagmap_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit-preview') {
    $form_state['storage'] = $form_state['values'];
  }
  if ($form_state['clicked_button']['#id'] == 'edit-submit-save') {
    if (user_access('administer views')) {
      $form['serialized']['#value'] = serialize($form_state['storage']); //for embed admin
      $build_id = $form_state['values']['form_build_id'];
      $_SESSION["$build_id"] = $form;
      unset($form_state['storage']);
      $form_state['redirect'] = array('vidi/view_generation', "build_id=$build_id&source=tagmap");
    }
  }
}

function vidi_tagmap_form_validate(&$form, &$form_state) {

  if (!is_numeric($form_state['values']['steps'])) {
    form_set_error('style_options][steps', t('Field @field must be integer.', array('@field' => $form['steps']['#title'])));
  }
  elseif ($form_state['values']['steps'] <= 0) {
    form_set_error('style_options][steps', t('Field @field must be positive value.', array('@field' => $form['steps']['#title'])));
  }

  if (!is_numeric($form_state['values']['fontsize'])) {
    form_set_error('style_options][fontsize', t('Field @field must be numeric.', array('@field' => $form['fontsize']['#title'])));
  }
  elseif ($form_state['values']['fontsize'] <= 0) {
    form_set_error('style_options][fontsize', t('Field @field must be positive value.', array('@field' => $form['fontsize']['#title'])));
  }

  if (!tagmap_css_dimensions_validate($form_state['values']['width'])) {
    form_set_error('style_options][map_settings][width', t('The specified value is not a valid CSS dimension.'));
  }

  if (!tagmap_css_dimensions_validate($form_state['values']['height'], TRUE)) {
    form_set_error('style_options][map_settings][height', t('The specified value is not a valid CSS dimension or is a percentage.'));
  }
}